/*
* Copyright (C) 2022 Texas Instruments Incorporated - http://www.ti.com/
*
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
#include <QDebug>
#include <termios.h>
#include "serialreaderthread.h"
#include "mmwgesturein.h"

/**
 * @brief Print a string as hexadecimal; useful for debugging the serial interface
 * 
 * @param str 
 * @param size 
 */
void printStringAsHex(char* str, int size)
{

    int pos = 0;


    printf("Printing %d byte hex string:", size);
    while(pos < size)
    {
        if (pos % 16 == 0)
        {
            printf("\r\n0x: ");
        }
        else if (pos % 2 == 0)
        {
            printf("  ");
        }
        
        printf("%02x", (uint8_t) str[pos]);
        pos++;
    }
    printf("\r\n");
}

/**
 * @brief  Helper function for print formatting of strings of hex that are read through the QSerialPort
 * 
 * 
 */
void printQStringHex(QByteArray str)
{
    int size;
    char* str_data;

    size = str.size();
    str_data = str.data();

    printStringAsHex(str_data, size);
}

/**
 * @brief Print out the probabilities reported for each gesture
 * 
 * @param gesture_probs 
 */
void printGestureProbabilities(mmwave_9_gesture_prob_t gesture_probs) {
    qDebug() << "Gesture Probilities:\nNull\t\tL2R\t\tR2L\t\tU2D\t\tD2U";
    qDebug() << gesture_probs.prob_null << "\t\t" << gesture_probs.prob_l2r << "\t\t" << gesture_probs.prob_r2l << "\t\t" << gesture_probs.prob_u2d << "\t\t" << gesture_probs.prob_d2u;
    qDebug() << "CW\t\tCCW\t\tOFF\t\tON\t\tSHINE";
    qDebug() << gesture_probs.prob_cw << "\t\t" << gesture_probs.prob_ccw << "\t\t" << gesture_probs.prob_off << "\t\t" << gesture_probs.prob_on << "\t\t" << gesture_probs.prob_shine;
}

/**
 * @brief Convert the raw byte string from UART for a floating point into the proper type
 * 
 * @param raw_prob little-endian byte-wise representation of single prevision float
 * @return float single precision floating-point representation of confidence percentage
 */
float uartProbToFloat(char raw_prob[4])
{

    uint32_t temp;
    float prob;

    memcpy(&temp, raw_prob, 4);
    uint32_t float_as_int =  le32toh((temp));
    memcpy(&prob, &float_as_int, 4);
    
    return prob;
}

/**
 * @brief Construct a new Serial Reader Thread:: Serial Reader Thread object
 * 
 * @param onlyProximity This sensor is only used to report proximity. It does not report gesture information -- this is for a dual-sensor setup where one is for proximity and one is for gesture detection.
 */
SerialReaderThread::SerialReaderThread(bool onlyProximity)
{
    m_privmode = GESTURE_MODE_IDLE;
    m_runSerialThread = true;
    m_onlyProximity = onlyProximity;
    m_serialPortName = "/dev/ttyUSB1";

    qDebug() << "Starting GUI early for testing purposes\n";
}

/**
 * @brief Destroy the Serial Reader Thread:: Serial Reader Thread object. Releases the serial port
 * 
 */
SerialReaderThread::~SerialReaderThread(void)
{
    qDebug() << "SerialReaderThread object is being deleted\n";
    m_serialPort->close();
    delete m_serialPort;
}

/**
 * @brief Set the name of the serial port to access. This is most often "/dev/ttyUSB1" on linux machines
 * 
 * @param port_name 
 */
void SerialReaderThread::setSerialPortName(QString port_name)
{
    qDebug() << "Update serial port to: " << port_name;
    m_serialPortName = port_name;
}

/**
 * @brief Create the serial port to the mmwave radar sensor's data port.
 * 
 * @return int return code. Negative is failure, 1 is correct
 */
int SerialReaderThread::createSerialPort()
{
    /* Open the serial port. Serail port property gets set by SerialReaderThread Object */
    m_serialPort = new QSerialPort;
    // If fails, perhaps give user prompt to type path to serial device
    // QString serialPortName = "/dev/ttyACM1";// "XDS110 Class Auxiliary Data Port";
    m_serialPort->setPortName(m_serialPortName);

    if (!m_serialPort->open(QIODevice::ReadOnly)) {
        qDebug() << QObject::tr("Failed to open port %1, error: %2").arg(m_serialPortName).arg(m_serialPort->errorString()) << endl;
        return -1;
    }

    m_serialPort->setBaudRate(921600, QSerialPort::Input); // user port is 115200, data is 921600
    m_serialPort->setDataBits(QSerialPort::Data8);
    m_serialPort->setParity(QSerialPort::NoParity);
    m_serialPort->setStopBits(QSerialPort::OneStop);
    m_serialPort->setFlowControl(QSerialPort::NoFlowControl);

    qDebug() << "Serial port opened successfully";
    return 1;
}

/**
 * @brief Format the data pulled from UART into a set of probabilities for the 9 classes (and 1 null class) recognized by the ANN running on the mmWave DSP.
 * 
 * @param uart_data 
 * @return mmwave_9_gesture_prob_t 
 */
mmwave_9_gesture_prob_t SerialReaderThread::formatGestureData(mmwave_9_gesture_uartdata_t * uart_data)
{
    mmwave_9_gesture_prob_t gesture_probs;

    gesture_probs.prob_null = uartProbToFloat(uart_data->gesture_data.ann_probabilities.prob_null);
    gesture_probs.prob_l2r = uartProbToFloat(uart_data->gesture_data.ann_probabilities.prob_l2r);
    gesture_probs.prob_r2l = uartProbToFloat(uart_data->gesture_data.ann_probabilities.prob_r2l);
    gesture_probs.prob_u2d = uartProbToFloat(uart_data->gesture_data.ann_probabilities.prob_u2d);
    gesture_probs.prob_d2u = uartProbToFloat(uart_data->gesture_data.ann_probabilities.prob_d2u);
    gesture_probs.prob_cw = uartProbToFloat(uart_data->gesture_data.ann_probabilities.prob_cw);
    gesture_probs.prob_ccw = uartProbToFloat(uart_data->gesture_data.ann_probabilities.prob_ccw);
    gesture_probs.prob_off = uartProbToFloat(uart_data->gesture_data.ann_probabilities.prob_off);
    gesture_probs.prob_on = uartProbToFloat(uart_data->gesture_data.ann_probabilities.prob_on);
    gesture_probs.prob_shine = uartProbToFloat(uart_data->gesture_data.ann_probabilities.prob_shine);

    return gesture_probs;
}

/**
 * @brief Select a gesture as the one performed by the user by comparing against a set of probabilities
 * 
 * @param gesture_probabilities The probability of each gesture based on ANN classifier
 * @param gesture_thresholds The lower threshold for recognition for each gesture 
 * @return Gesture_gesture_type The gesture that has been recognized/classified 
 */
Gesture_gesture_type SerialReaderThread::selectGesture(mmwave_9_gesture_prob_t gesture_probabilities, mmwave_9_gesture_prob_t gesture_thresholds)
{
    Gesture_gesture_type selected_ges = GESTURE_TYPE_NONE;
    float highest_prob = 0.0;

    if (gesture_probabilities.prob_null > highest_prob && 
        gesture_probabilities.prob_null > gesture_thresholds.prob_null)
    {
        selected_ges = GESTURE_TYPE_NONE;
        highest_prob = gesture_probabilities.prob_null;   
    }
    if (gesture_probabilities.prob_l2r > highest_prob && 
        gesture_probabilities.prob_l2r > gesture_thresholds.prob_l2r)
    {
        selected_ges = GESTURE_TYPE_LEFT_TO_RIGHT;
        highest_prob = gesture_probabilities.prob_l2r;   
    }
    if (gesture_probabilities.prob_r2l > highest_prob && 
        gesture_probabilities.prob_r2l > gesture_thresholds.prob_r2l)
    {
        selected_ges = GESTURE_TYPE_RIGHT_TO_LEFT;
        highest_prob = gesture_probabilities.prob_r2l;   
    }
    if (gesture_probabilities.prob_u2d > highest_prob && 
        gesture_probabilities.prob_u2d > gesture_thresholds.prob_u2d)
    {
        selected_ges = GESTURE_TYPE_UP_TO_DOWN;
        highest_prob = gesture_probabilities.prob_u2d;   
    }
    if (gesture_probabilities.prob_d2u > highest_prob && 
        gesture_probabilities.prob_d2u > gesture_thresholds.prob_d2u)
    {
        selected_ges = GESTURE_TYPE_DOWN_TO_UP;
        highest_prob = gesture_probabilities.prob_d2u;   
    }
    if (gesture_probabilities.prob_cw > highest_prob && 
        gesture_probabilities.prob_cw > gesture_thresholds.prob_cw)
    {
        selected_ges = GESTURE_TYPE_TWIRL_CW;
        highest_prob = gesture_probabilities.prob_cw;   
    }
    if (gesture_probabilities.prob_ccw > highest_prob && 
        gesture_probabilities.prob_ccw > gesture_thresholds.prob_ccw)
    {
        selected_ges = GESTURE_TYPE_TWIRL_CCW;
        highest_prob = gesture_probabilities.prob_ccw;   
    }
    if (gesture_probabilities.prob_off > highest_prob && 
        gesture_probabilities.prob_off > gesture_thresholds.prob_off)
    {
        selected_ges = GESTURE_TYPE_OFF;
        highest_prob = gesture_probabilities.prob_off;   
    }
    if (gesture_probabilities.prob_on > highest_prob && 
        gesture_probabilities.prob_on > gesture_thresholds.prob_on)
    {
        selected_ges = GESTURE_TYPE_ON;
        highest_prob = gesture_probabilities.prob_on;   
    }    
    if (gesture_probabilities.prob_shine > highest_prob && 
        gesture_probabilities.prob_shine > gesture_thresholds.prob_shine)
    {
        selected_ges = GESTURE_TYPE_SHINE;
        highest_prob = gesture_probabilities.prob_shine;   
    }

    return selected_ges;
}


/**
 * @brief Consume bytes from the serial port and report the detected gesture and user's proximity at an application level to other logic in the program (HMILogic)
 * 
 * @param ba A byte array containing the raw data received over serial from the mmwave radar board. Assumed to follow the format described in the 9-gesture demo
 */
void SerialReaderThread::interpretGestureData(QByteArray * ba)
{
    static Gesture_gesture_type last_gesture = GESTURE_TYPE_NONE;
    static int gesture_debounce = 0;


    if (ba->size() == MMWAVE_9_GESTURE_MSG_LENGTH)
    {
        mmwave_9_gesture_uartdata_t ges_data;
        Gesture_gesture_type detected_gesture;

        static qint64 last_t = 0;
        qint64 t;

        t = QDateTime::currentMSecsSinceEpoch();


        memcpy(&(ges_data.bytearray), (ba->data()), MMWAVE_9_GESTURE_MSG_LENGTH);
        mmwave_9_gesture_prob_t gesture_probabilities; 
        
        gesture_probabilities = formatGestureData(&ges_data);
        detected_gesture = selectGesture(gesture_probabilities, GESTURE_CONFIDENCE_THRESHOLDS);

        if (detected_gesture != last_gesture && 
            ((t - last_t) > GESTURE_DEBOUNCE_TIMEOUT_MS || last_t > t) ) {
            // gesture_debounce <= 0) {
            emit detectGesture(detected_gesture);


            // We may have a wave of gestures recognized that don't help us, in addition to more of the same gesture. 'Debounce' the signal
            if (detected_gesture != GESTURE_TYPE_SHINE && detected_gesture != GESTURE_TYPE_NONE) {
                //Detections may oscillate between gesture and NONE
                // gesture_debounce = GESTURE_DEBOUNCE_TIMEOUT;
                last_t = t;
            }
            last_gesture = detected_gesture;

        } else{
            if (detected_gesture != GESTURE_TYPE_SHINE && detected_gesture != GESTURE_TYPE_NONE) {
                emit detectProximity(true);
            }
            else {
                emit detectProximity(false);
            }

        }
        // gesture_debounce = std::max(gesture_debounce-1, 0);

    }

}

/**
 * @brief Interpret from the raw data over serial whether there is a user in proximity or not. This is from a simplified gesture recognition (2 gestures) and proximity detection demo on ti.com
 * 
 * @param ba 
 */
void SerialReaderThread::interpretProximityData(QByteArray * ba)
{
    Gesture_proximity_type proximity = (Gesture_proximity_type) ba->at(0);
    switch (proximity) {
        case PROXIMITY_NO_MOTION:
            emit detectProximity(false);

        break;
        case PROXIMITY_0_5m:
        case PROXIMITY_1m:
        case PROXIMITY_2m:
        case PROXIMITY_3m:
            emit detectProximity(true);

        break;
        default:
            QString data = QString::fromLocal8Bit(*ba);
            qDebug() << "Unexpected proximity/gesture reading from sensor: " << data;
    }
}

/**
 * @brief Set the main run-loop of the thread to exit
 * 
 */
void SerialReaderThread::handleQuit()
{
    m_runSerialThread = false;
}

/**
 * @brief The top level function of the serial reader. This is automatically called when the thread is started, and will continuously read data over serial from the mmwave sensor.
 * 
 */
void SerialReaderThread::run() {
    printf("inside serialthread run\n");
    //Need to create serialport in this thread else QT application throws error.
    // QT doesn't allow QSocket to be accessed from two different threads (main thread and serial thread)
    // Get inside while loop and attempt to read the bytes only if the serial port was opened successfully
    if(createSerialPort() > 0) {
        
        while(m_runSerialThread == true) {
            if ( m_serialPort->bytesAvailable() > 0 || m_serialPort->waitForReadyRead(10)) {
                QByteArray ba;
                ba=m_serialPort->readAll();

                // printf("Read %d bytes: %s\r\n", ba.size(), ba.data());
                // printQStringHex(ba);
                if (m_onlyProximity) {
                    interpretProximityData(&ba);
                }
                else {
                    interpretGestureData(&ba);
                }

                msleep(1);
            }
        }
    }
    emit srtQuit();
    qDebug() << "Serial reader thread quit\n";
}
