/*
* Copyright (C) 2022 Texas Instruments Incorporated - http://www.ti.com/
*
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#include "door_control.h"

const uint16_t WILNK_DEFAULT_PORT = 6254;
const uint16_t NULL_PORT = 0;
const char WILINK_DEFAULT_IP_ADDR[16] = "192.168.43.150";
const char NULL_IP_ADDR[16] = "0.0.0.0";
const uint16_t RGB_BUFF_SIZE = 3;
const uint8_t RED_INDEX = 0;
const uint8_t GREEN_INDEX = 1;
const uint8_t BLUE_INDEX = 2;

/**
 * @brief Check whether the input socket is connected or not
 * 
 * @param sockfd a socket file descriptor
 * @return true The socket is connected and has not indicated any error
 * @return false The socket is not connected, has indicated error, or does not exist
 */
bool DoorControl::isConnected(int sockfd)
{
    int error_code;
    bool c = false;
    int error_code_size = sizeof(error_code);

    if (no_connect) return false;

    qDebug() << "DoorControl:: Check connection...";

    getsockopt(sockfd, SOL_SOCKET, SO_ERROR, &error_code, (socklen_t*) &error_code_size);

    if (error_code != 0)
    {
        qDebug() << "DoorControl:: Socket gives error code: " << error_code;
    }

    c = ((!error_code) && is_connected && sockfd != -1);
    qDebug() << "DoorControl:: Socket is connected: " << c;
    return c;
}

/**
 * @brief Construct a new Door Control:: Door Control object
 * 
 * @param ip The IP address of the wilink MCU
 * @param port The port for that the TCP server is accepting connections from
 */
DoorControl::DoorControl(const char ip[16], const int port)
{
    is_connected = false;
    no_connect = false;
    //if a null address was provided, then never try to connect or send information to a light
    if (port == NULL_PORT && strcmp(NULL_IP_ADDR, ip) == 0)
    {
        qDebug() << "Null IP and port; do not connect to any light";
        no_connect = true;
    }

    qDebug() << "Connect to wifi light at " << ip << ":" << port << " (ip len = " << strlen(ip) << ")";

    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(ip);
    servaddr.sin_port = htons(port);

    createSocket();
    if (sockfd != -1) {
        printf("DoorControl:: Socket successfully created\n");
        if (connectToDoor())
        {
            doorLightIdle();
        }
    }
}

bool DoorControl::isUsingDoorLight()
{
    return !no_connect;
}

/**
 * @brief Destroy the Door Control:: Door Control object
 * 
 */
DoorControl::~DoorControl()
{
    shutdownDoorLight();
    close(sockfd);
}

/**
 * @brief Create a TCP socket. Does not yet set an address
 * 
 * @return int The socket file descriptor. This is value is also set to sockfd.
 */
int DoorControl::createSocket()
{
    if (no_connect) return -1;

    // sockfd = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1) {
        qDebug() << "DoorControl:: socket creation failed..";
    }
    return sockfd;
}

/**
 * @brief Connect the socket for the address setup in the constructor
 * 
 * @return true The socket connected successfully
 * @return false The socket is not connected
 */
bool DoorControl::connectToDoor()
{
    bool ret;
    int err;

    if (no_connect) return false;

    qDebug() << "DoorControl:: Setup connection...";

    if (sockfd == -1) {
        createSocket();
    }

    err = ::connect(sockfd, (const sockaddr*) &servaddr, sizeof(servaddr));
    if (err != 0) {
        qDebug() << "DoorControl:: connection with the server failed... " << err;
        ret = false;
        is_connected = false;
        close(sockfd);
        sockfd = -1;
    }
    else {
        qDebug() << "DoorControl:: Connected to " << WILINK_DEFAULT_IP_ADDR;
        ret = true;
        is_connected = true;
    }

    return ret;

}

/**
 * @brief slot to set the door light to indicate the door is unlocked (color green)
 * 
 */
void DoorControl::doorLightUnlocked()
{
    bool do_send = true;
    
    if (!isConnected(sockfd)) do_send = connectToDoor();

    if (do_send) {

        qDebug() << "DoorControl:: sending command to door; unlocked";
        char buff[RGB_BUFF_SIZE] = {0, 0, 0};
        buff[GREEN_INDEX] = 0xC0;

        write(sockfd, buff, sizeof(buff));
    }

}

/**
 * @brief Slot to set the door light to indicate the door is actively locked (color red)
 * 
 */
void DoorControl::doorLightLocked()
{
    bool do_send = true;
    if (!isConnected(sockfd)) do_send = connectToDoor();

    if (do_send) {

        qDebug() << "DoorControl:: Sending command to door; locked";
        char buff[RGB_BUFF_SIZE] = {0, 0, 0};
        buff[RED_INDEX] = 0xC0;

        write(sockfd, buff, sizeof(buff));
    }
}

/**
 * @brief Slot to set the door light to indicate that the code has been put in correctly, but we are waiting for a user to take their ID badge picture (color yellow);
 * 
 */
void DoorControl::doorLightAwaitingVisitor()
{
    bool do_send = true;
    if (!isConnected(sockfd)) do_send = connectToDoor();

    if (do_send) {

        qDebug() << "DoorControl:: Sending command to door; await visitor"
        ;
        char buff[RGB_BUFF_SIZE] = {0, 0, 0};
        buff[GREEN_INDEX] = 0xC0;
        buff[RED_INDEX] = 0xC0;
        write(sockfd, buff, sizeof(buff));
    }
}

/**
 * @brief Slot to set the door light to indicate that the system is idle and locked (color white).
 * 
 */
void DoorControl::doorLightIdle()
{
    bool do_send = true;
    if (!isConnected(sockfd)) do_send = connectToDoor();

    if (do_send) {

        qDebug() << "DoorControl:: Sending command to door; idle";
        char buff[RGB_BUFF_SIZE] = {0x50, 0x50, 0x50};

        write(sockfd, buff, sizeof(buff));
    }
}

/**
 * @brief Shutdown the door light as the system exits (color dull white)
 * 
 */
void DoorControl::shutdownDoorLight()
{
    bool do_send = true;
    if (!isConnected(sockfd)) do_send = connectToDoor();

    if (do_send) {

        char buff[RGB_BUFF_SIZE] = {0x10, 0x10, 0x10};

        write(sockfd, buff, sizeof(buff));
    }
}