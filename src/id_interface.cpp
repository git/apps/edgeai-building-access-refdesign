/*
* Copyright (C) 2022 Texas Instruments Incorporated - http://www.ti.com/
*
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#include "id_interface.h"



/**
 * @brief Construct a new IDInterface::IDInterface object
 * 
 * @param object The object which has parameters to control the visibility of ID-card related elements, namely the camera feed and the visitor badge temporarily displayed on the screen. This object needs to be boolean properties for 'isfacedetected' and 'showcamera'.
 */

IDInterface::IDInterface(QObject *object){

    main_object = object;
}

/**
 * @brief Reset the interface for the ID card/face detection by turning off their visibility
 * 
 */
void IDInterface::resetIDInterface(){
    main_object->setProperty("isfacedetected", false);
    main_object->setProperty("showcamera", false);
}


/**
 * @brief Slot for which a signal arrival means that an ID card / visitor badge has been created and is ready to be shown. 
 * 
 */
void IDInterface::createIDCard(){
    qDebug() << "Creating ID card";
    main_object->setProperty("isfacedetected", true);
    emit badgeProduced();
    emit finishedCreatingID(false);

}


/**
 * @brief Slot that controls whether the camera feed (with face detection) is to be shown or not.  
 * 
 * @param lockUnlocked if the lock is unlocked (true), then the camera's feed should be shown. Else, it is hidden 
 */
void IDInterface::readyToTakeImage(bool lockUnlocked){

    if (lockUnlocked)
    {
        main_object->setProperty("showcamera", true);
    }
    else
    {
        main_object->setProperty("showcamera", false);
    }
}

//functions to add text for access granted, input passcode, etc.