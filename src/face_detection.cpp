/*
* Copyright (C) 2022 Texas Instruments Incorporated - http://www.ti.com/
*
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#include "face_detection.h" 

//path to the XML file that describes the cascade filter used for face detection
const char* FACE_MODEL_PATH = "facemodels/haarcascade_frontalface_default.xml";
//Minimum delay between turning on the camera and taking an image for the ID
const qint64 TAKE_ID_IMAGE_DELAY_MSEC = 4 * 1000; 
//Number of additional pixels in each direction to include when cropping the original image down to only include the face
const int FACE_IMAGE_EXTRA_PX = 50;
//Used to signal that face detection has not yet started
const qint64 NULL_START_TIME = -1;

/**
 * @brief Convert an image from QT format (QVideoFrame) to openCV's Mat format, specifically as an RGB image
 * 
 * @param input A QVideoFrame provided by the video filter. The format will depend on the camera being used. During development, a Logitech C270 camera was used, which provides images in YUYV format w/ 8-bits per channel. **The input must already be "mapped" for input->bits to return valid data**
 * @return cv::Mat RGB image with the same x,y dimensions as the input
 */
cv::Mat getRGBImageFromFrame(QVideoFrame* input)
{
    int input_h, input_w;
    uchar* image_data;
    cv::Mat rgb;

    input_h = input->height();
    input_w = input->width();

    image_data = input->bits(); //This should have already been 'mapped'

    if (input->pixelFormat() == QVideoFrame::Format_YUYV){
        /* Convert image into opencv format*/
        cv::Mat yuyv(input_h, input_w, CV_8UC2, image_data); 

        /* Convert image to RGB */
        rgb = cv::Mat(input_h, input_w, CV_8UC3);
        cv::cvtColor(yuyv, rgb, cv::COLOR_YUV2RGB_YUYV);
    }
    else if (input->pixelFormat() == QVideoFrame::Format_UYVY) {
        /* Convert image into opencv format*/
        cv::Mat yuyv(input_h, input_w, CV_8UC2, image_data); 

        /* Convert image to RGB */
        rgb = cv::Mat(input_h, input_w, CV_8UC3);
        cv::cvtColor(yuyv, rgb, cv::COLOR_YUV2RGB_UYVY);
    }
    else if (input->pixelFormat() == QVideoFrame::Format_RGB24)
    {
        rgb = cv::Mat(input_h, input_w, CV_8UC3, image_data);
    }
    else {
        qDebug() << "FaceDetectionFilter failed to read image due to unexpected format: " << input->pixelFormat();
    }

    return rgb;
}

/**
 * @brief Construct a new Face Detect Filter Runnable to handle face detection on video inputs. This will load the cascade model used for face detection
 * 
 * @param creator The handle of the QAbstractVideoFilter that created this runnable. This should be castable to a FaceDetectFilter
 */
FaceDetectFilterRunnable::FaceDetectFilterRunnable(QAbstractVideoFilter* creator){
    filter = creator;
    cascade.load(FACE_MODEL_PATH);

}

/**
 * @brief The top level function of the runnable filter, called on every new frame
 * 
 *      If face detection is enabled (using a setter function), this will convert the input image to RGB and do face detection with openCV. It will draw an faces on the images shown in the live-feed in QML. After it has run for a few seconds, it will take a final image of the face to be cropped and displayed with a example visitor badge / ID card.
 * 
 *      This function can block the GUI from updating, so fast runtime is desireable
 * 
 * @param input The input image (QVideoFrame*)
 * @param surfaceFormat Not used
 * @param flags Not used
 * @return QVideoFrame 
 */
QVideoFrame FaceDetectFilterRunnable::run(QVideoFrame *input, const QVideoSurfaceFormat &surfaceFormat, RunFlags flags)
{
    QVideoFrame output;
    double scale_x = 0.4, scale_y = 0.4; //scale factors for the image before face detection. >0.5 results in noticeable latency in the GUI.

    Q_UNUSED(flags); 
    Q_UNUSED(surfaceFormat);

    //default output should simply be the input
    output = *input; 

    // input->image().save("input image.png");

    //The image must be mapped to access the memory within; returns true on success
    bool image_mapped = input->map(QAbstractVideoBuffer::ReadOnly);

    if (image_mapped){
        cv::Mat rgb;        

        rgb = getRGBImageFromFrame(input);

        if (!rgb.empty()) {
            /* Flip image along right/left axis to seem more like a mirror when displayed live*/
            cv::flip(rgb, rgb, 1);
            /*  Face Detection using conventional OpenCV mechanisms */
            if (doFaceDetection) {
                std::vector<cv::Rect> faces = detectFaces(rgb, scale_x, scale_y);

                /* Write bounding box (if present) to image */
                if (faces.size() > 0)
                {
                    // Cloning the image seems essential -- without, the image displayed is effectively junk data. 
                    processDetectedFace(rgb.clone(), faces.at(0), scale_x, scale_y);
                }

                /* Draw a white box around the detected face */
                for (cv::Rect face : faces)
                {
                    cv::Rect rect(face.tl() / scale_x, face.br() / scale_y);
                    cv::rectangle(rgb, rect, cv::Scalar(255, 255, 255), 10);
                }
            }


            /* Convert from opencv format back into QT image/video frame format*/
            QImage temp_img(rgb.data, rgb.cols, rgb.rows, QImage::Format_RGB888);
            temp_img.convertTo(QVideoFrame::imageFormatFromPixelFormat(QVideoFrame::Format_RGB32));
            output = QVideoFrame(temp_img);

            //unmap frees associated memory
            input->unmap();
            // output->image().save("output image.png");

        }
    }

    return output;
}

/**
 * @brief Run face detection using conventional methods (cascade classifier)
 * 
 * @param rgb_image an RGB image containing faces to recognize 
 * @param scale_x A double representing the scale to resize the image by in x-dimension. <1 results in a smaller image, >1 in a larger one.
 * @param scale_y A double representing the scale to resize the image by in x-dimension
 * @return std::vector<cv::Rect> A vector of rectangles representing where faces were detected
 */
std::vector<cv::Rect> FaceDetectFilterRunnable::detectFaces(cv::Mat rgb_image, double scale_x, double scale_y)
{
    std::vector<cv::Rect> faces;
    cv::Mat gray_image, small_image;

    //classification parameters
    int num_rectangles_needed = 5, flags = 0x0;
    double scale_per_step = 1.3;
    cv::Size min_size(30,30);

    // convert to grayscale and resize based on scale factors
    cv::cvtColor(rgb_image, gray_image, cv::COLOR_RGB2GRAY);
    cv::resize(gray_image, small_image, cv::Size(), scale_x, scale_y, cv::INTER_LINEAR);
    // represent the image with a histogram before classication
    cv::equalizeHist(small_image, small_image);

    // run classification / face detection using the loaded cascade-model
    cascade.detectMultiScale(small_image, faces, scale_per_step,num_rectangles_needed , flags, min_size);

    return faces;
}

/**
 * @brief send an image through QT to update the face shown in the GUI. This will be cropped to include the face and then resized to fit the dimensions present in the QTQuick.Image that displays the image. 
 * 
 *      Some dimensions to resize to cause images when converting from cv::Mat to QImage (a shifting of rows that makes the image look like it's been pushed to create a slanted version that wraps at the edges). The only consistent solution has been to modify the dimensions prior to converting to QImage. This may require tuning in the QML file for the Image's dimensions.
 * 
 * @param image The image face detection was run on
 * @param face_dims A cv::Rect representing the location of the face
 */
void FaceDetectFilterRunnable::sendFaceImage(cv::Mat image, cv::Rect face_dims, FaceImageProvider* provider)
{
    int width, height;
    cv::Mat face_image, fitted_face_image;

    width = image.size().width;
    height = image.size().height;

    /* Add extra space to each side of the face so that more of the person's head is shown */
    face_dims.x = std::max(face_dims.x - FACE_IMAGE_EXTRA_PX, 0);
    face_dims.y = std::max(face_dims.y - FACE_IMAGE_EXTRA_PX, 0);
    face_dims.width = face_dims.width + FACE_IMAGE_EXTRA_PX * 2;
    face_dims.height = face_dims.height + FACE_IMAGE_EXTRA_PX * 2;

    /* if the dimensions would exceed the image, force them back into the image size */
    if (face_dims.x + face_dims.width >= width) {
        face_dims.width = width - face_dims.x - 1;
    }
    if (face_dims.y + face_dims.height >= height) {
        face_dims.height = height - face_dims.y - 1;
    }

    //ensure dimensions are square and Even # of pixels (else QImage conversion inserts strange diagonal stitching/copying)
    face_dims.height = std::min(face_dims.height, face_dims.width);
    face_dims.width = std::min(face_dims.height, face_dims.width);

    // Crop the image to the rectangle's dimensions and resize to fit the Image in the GUI
    face_image = image(face_dims);
    cv::resize(face_image, fitted_face_image, cv::Size(provider->getImageWidth(), provider->getImageHeight()), cv::INTER_LINEAR);

    // Convert to a QImage. If a slanted image is being shown, it's likely happening at this line. There appears a strong correlation to the image dimensions, which can be modified in the QML file. The root cause is not identified.
    QImage* temp_img = new QImage(fitted_face_image.data, fitted_face_image.cols, fitted_face_image.rows, QImage::Format_RGB888);
    // qDebug() << &temp_img;

    //update the image held by the provider, which will display this image in the GUI
    provider->updateImage(temp_img);

}

/**
 * @brief Handle the singular face (represented as a cv::Rect) by sending it to be displayed in the GUI and signalling to the rest of the program that a face was detected
 * 
 * @param image The RGB image containing the detected face
 * @param face cv::Rect containing the location of the face
 * @param scale_x a double representing the scale by which the image was resized in x -dimension when running face detection; the 'face' needs this operation reversed
 * @param scale_y a double representing the scale by which the image was resized in y -dimension when running face detection; the 'face' needs this operation reversed
 */
void FaceDetectFilterRunnable::processDetectedFace(cv::Mat image, cv::Rect face, double scale_x, double scale_y)
{
    /* Allow delay before signalling face-detection with a final image */
    if (QDateTime::currentMSecsSinceEpoch() - timeStartFaceDetectMsec > TAKE_ID_IMAGE_DELAY_MSEC )
    {
        cv::Rect face_rect(face.tl() / scale_x, face.br() / scale_y);
        sendFaceImage(image, face_rect, faceImageProvider);
        //signal the face was detected
        emit ((FaceDetectFilter*)filter)->faceDetected();
    }
}

/** Getters and Setters **/
/**
 * @brief Control whether face detection should be running. If true, capture the current time so we can insert a delay between starting face detection and capturing an image for display
 * 
 * @param enable Whether to start (true) or stop (false) detecting faces
 */
void FaceDetectFilterRunnable::startDetecting(bool enable) {
    doFaceDetection = enable; 
    if (enable) timeStartFaceDetectMsec = QDateTime::currentMSecsSinceEpoch();
    else		timeStartFaceDetectMsec = NULL_START_TIME;
} 

/**
 * @brief Get a boolean representing whether face detection is running or not
 * 
 * @return true Face Detection is running
 * @return false Face Detection is not running
 */
bool FaceDetectFilterRunnable::isDetecting() {
    return doFaceDetection;
}

/**
 * @brief Set the image provider that the filter-runnable should push new images to for display
 * 
 * @param fip A FaceImageProvider that is attached to a QTQuick.Image QML object. 
 */
void FaceDetectFilterRunnable::setImageProvider(FaceImageProvider* fip){
    faceImageProvider = fip;
}

/**
 * @brief Get the image orivuder
 * 
 * @return FaceImageProvider* 
 */
FaceImageProvider* FaceDetectFilterRunnable::getFaceImageProvider(){
    return faceImageProvider;
}

/**
 * @brief Create the runnable for the FaceDetectFilter. Establishes members like the image provider. By default, face detection is not running when this is created.
 * 
 * @return QVideoFilterRunnable* A FaceDetectFilterRunnable* that will run each time a frame is captured from the video input (typically a camera)
 */
QVideoFilterRunnable* FaceDetectFilter::createFilterRunnable()
{
    qDebug() << "Creating filter";
    faceDetectFilterRunnable = new FaceDetectFilterRunnable(this);
    setRunFaceDetection(false);
    faceDetectFilterRunnable->setImageProvider(faceImageProvider);
    return faceDetectFilterRunnable;
}

/**
 * @brief Enables or disables face detection within the runnable
 * 
 * @param enable 
 */
void FaceDetectFilter::setRunFaceDetection(bool enable) { 
    faceDetectFilterRunnable->startDetecting(enable); 
}

/**
 * @brief Sets the image provider for this object and it's child FaceDetectFilterRunnable*. The child is the only one that uses this object, but it is easier to interact with the filter object than its runnable.
 * 
 * @param fip 
 */
void FaceDetectFilter::setImageProvider(FaceImageProvider* fip){
    qDebug() << "setting image provider";
    faceImageProvider = fip;
    if (faceDetectFilterRunnable)
    {
        qDebug() << "set for runnable";
        faceDetectFilterRunnable->setImageProvider(faceImageProvider);
    }
}

/**
 * @brief Get the image provider
 * 
 * @return FaceImageProvider* 
 */
FaceImageProvider* FaceDetectFilter::getFaceImageProvider(){
    return faceImageProvider;
}

/**
 * @brief returns whether face detection is running or not. Simply a proxy for the runnable's equivalent getter.
 * 
 */
bool FaceDetectFilter::isRunningFaceDetect() { 
    return faceDetectFilterRunnable->isDetecting(); 
}

/**
 * @brief Controls whether face detection is enabled or disabled. Effectively a proxy for the runnable's equivalent setter.
 * 
 * @param enable 
 */
void FaceDetectFilter::controlFaceDetection(bool enable)
{
    setRunFaceDetection(enable);

}