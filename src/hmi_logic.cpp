/*
* Copyright (C) 2022 Texas Instruments Incorporated - http://www.ti.com/
*
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#include "hmi_logic.h"

const qint64 NO_GESTURE_TIMEOUT_MSEC = 60*1000;

/**
 * @brief Determine whether a gesture is capable of activating the GUI or not 
 * 
 * @param ges The gesture performed
 * @return true The gesture should wake up the application if it is idled
 * @return false The gesture should not wake up the application
 */
bool isActivationGesture(Gesture_gesture_type ges)
{
    bool result = false;
    switch (ges) {
        case GESTURE_TYPE_RIGHT_TO_LEFT:
        case GESTURE_TYPE_LEFT_TO_RIGHT:
        case GESTURE_TYPE_UP_TO_DOWN:
        case GESTURE_TYPE_DOWN_TO_UP:
            result = true;

        break;
        case GESTURE_TYPE_ON:
        case GESTURE_TYPE_OFF:
        case GESTURE_TYPE_TWIRL_CCW:
        case GESTURE_TYPE_TWIRL_CW:
        case GESTURE_TYPE_NONE:
        case GESTURE_TYPE_SHINE:
        default:
            result = false;
    }

    return result;
}

/**
 * @brief Construct a new HMILogic::HMILogic object and initialize all state variables to their defaults
 * 
 */
HMILogic::HMILogic() {

    lastPrecenseDetection = false;    //possibly replace with proximity-related enum
    lastGestureID = GESTURE_TYPE_NONE;
    lastCameraResult = CAM_NO_FACE_DETECTED; //replace with informative enum
    isUnlocked = false; 
    lastStateChangeTimeMsec = QDateTime::currentMSecsSinceEpoch();
}

/**
 * @brief Destroy the HMILogic::HMILogic object
 * 
 */
HMILogic::~HMILogic() {
    qDebug() << "HMILogic is being destroyed\n";

}

/**
 * @brief Slot: Handle quitting the thread by releasing any resources like open files or resources. There are none to release at this time
 * 
 */
void HMILogic::handleQuit() {
}

/**
 * @brief Slot: accept new information about motion being detected
 * 
 * @param motionDetected true if motion was detection; false otherwise
 */
void HMILogic::handleProximity(bool motionDetected) {
    lastPrecenseDetection = motionDetected;
    executeFSM(lastPrecenseDetection, GESTURE_TYPE_NONE, lastCameraResult, isUnlocked);
}

/**
 * @brief Slot: Invoked when the gesture changes. This will run the state machine if the gesture is different than the one before
 * 
 * 
 * @param gesture The most recently detected gesture (may by a null gesture) 
 */
void HMILogic::handleGesture(Gesture_gesture_type gesture) {
    if (lastGestureID != gesture) {
        lastGestureID = gesture;
        qDebug() << "HMI logic receives gesture" << (int) gesture;
        executeFSM(lastPrecenseDetection, lastGestureID, lastCameraResult, isUnlocked);
    }
}

/**
 * @brief Slot: accept updates to the lock state per keyword.cpp. This will run the state machine if the lock state has changed
 * 
 * @param unlocked true if the lock has been unlocked, false if it is still locked. 
 */
void HMILogic::updateLockState(bool unlocked) {
    if (isUnlocked != unlocked)
    {
        isUnlocked = unlocked;
        executeFSM(lastPrecenseDetection, lastGestureID, lastCameraResult, isUnlocked);
    }
}

/**
 * @brief Slot: called when face-detection has found a face; this will run the main FSM
 * 
 */
void HMILogic::handleUserAdded() {
    executeFSM(lastPrecenseDetection, lastGestureID, CAM_FACE_DETECTED, isUnlocked);

}

/**
 * @brief Slot used to emit another signal. This is generally called using a QTimer, which only connects to a single slot, although the desired purpose is to emit a signal that will arrive to multiple slots. This slot is effectively a proxy.
 * 
 */
void HMILogic::sendReset() {
    emit resetInterface(); //use this to move timer's signal from 1:1 to 1:many
}


void HMILogic::externalLock()
{

}

void HMILogic::externalUnlock()
{


}


/**
 * @brief Mealy Finite State Machine to interpret proximity, gesture, and camera inputs into changes for the lock and GUI state. Triggered by slots and will emit signals for going into idle/active state, checking lock code, reseting interfaces
 * 
 */
void HMILogic::executeFSM(bool motionDetected, Gesture_gesture_type gesture, CameraResult_t cameraResult, bool unlocked) {
    static HMI_super_state_t state = S_IDLE;
    static bool resetGestureLockFSM = false;

    HMI_super_state_t last_state = state;


    switch (state)
    {
        case S_IDLE:
            if (motionDetected || isActivationGesture(gesture))
            {
                emit wakeup(); 
                state = S_LOCK;
            }

        break;
        case S_LOCK:
            if (unlocked)
            {
                emit doorAwaitVisitor();
                state = S_ADD_USER;
            }
            else if ((!motionDetected || !isActivationGesture(gesture)) && (QDateTime::currentMSecsSinceEpoch() - lastStateChangeTimeMsec) > NO_GESTURE_TIMEOUT_MSEC)
            {
                emit idle();
                state = S_IDLE;
            } else
            {
                executeGestureLockFSM(gesture, resetGestureLockFSM);
                resetGestureLockFSM = false;
            }

        break;
        case S_ADD_USER:
            if (cameraResult == CAM_FACE_DETECTED)
            {
                qDebug() << "Face detected and user added; starting timer to reset interface";
                QTimer::singleShot(RESET_TIMER_DELAY_MSEC, this, SLOT(sendReset()));
                emit readyToUnlock();
                state = S_UNLOCKED;
            } else if ((!motionDetected || !isActivationGesture(gesture)) && (QDateTime::currentMSecsSinceEpoch() - lastStateChangeTimeMsec) > NO_GESTURE_TIMEOUT_MSEC) {
                emit sendReset();
                unlocked = false;
                state = S_IDLE;
                emit idle();
            }

        break;//Fallthrough until implemented
        case S_UNLOCKED:
            resetGestureLockFSM = true; //signal to reset the sub-state machine next time
            if (unlocked == false){
                emit doorLock();
                state = S_LOCK;
            } else if ((!motionDetected || !isActivationGesture(gesture))  && (QDateTime::currentMSecsSinceEpoch() - lastStateChangeTimeMsec) > NO_GESTURE_TIMEOUT_MSEC) {
                emit sendReset();
                unlocked = false;
                state = S_IDLE;
                emit idle();
            }

        break;
        default:
            qDebug() << "Unexpected state: " << state;
    }
    //If the state changes, we should treat this as user activity, which prevents idle-mode 
    if (state != last_state)
    {
        lastStateChangeTimeMsec = QDateTime::currentMSecsSinceEpoch();
        qDebug() << "HMILogicFSM:: state: " << last_state << "-> " << state << "; inputs:: motion: " << motionDetected << ", gesture: " << gesture << ", unlocked: " << unlocked;
    }

}

/**
 * @brief Mealy state machine operating within the S_LOCK state of the main FSM. This uses gestures to control the lock by changing the current digit index and the digit value using up, right, down, and left swiping gestures. The 'shine' gesture causes the lock code to be checked (NB: this gesture is not well trained and is often detected at the start or end of other gestures)
 * 
 * @param gesture The most recent gesture performed
 * @param reset forces the state machine to reset to the default state
 */
void HMILogic::executeGestureLockFSM(Gesture_gesture_type gesture, bool reset)
{
    static HMI_gesture_lock_state_t state = S_LOCK_NO_GESTURE;

    if (reset) 
    {
        state = S_LOCK_NO_GESTURE;
    }

    HMI_gesture_lock_state_t last_state = state;

    switch (state)
    {
        case S_LOCK_NO_GESTURE:
            // Main state; the state machine should generally be at this point as it waits for a new gesture
            // qDebug() << "Locked; received gesture " << gesture;
            if (gesture == GESTURE_TYPE_RIGHT_TO_LEFT)
            {
                state = S_LOCK_SHIFT_LEFT;
                emit shiftDigitLeft();
            }
            else if (gesture == GESTURE_TYPE_LEFT_TO_RIGHT)
            {
                state = S_LOCK_SHIFT_RIGHT;
                emit shiftDigitRight();
            }
            else if (gesture == GESTURE_TYPE_UP_TO_DOWN)
            {   
                state = S_LOCK_DECREMENT_DIGIT;
                emit decrementDigit();
            } else if (gesture == GESTURE_TYPE_DOWN_TO_UP)
            {
                state = S_LOCK_INCREMENT_DIGIT;
                emit incrementDigit();
            } else if (gesture == GESTURE_TYPE_SHINE)
            {
                state = S_LOCK_CHECK_CODE;
                emit attemptUnlock();
            }

        break;
        case S_LOCK_SHIFT_LEFT:
            if (gesture != GESTURE_TYPE_RIGHT_TO_LEFT)
                state = S_LOCK_NO_GESTURE;

        break;
        case S_LOCK_SHIFT_RIGHT:
            if (gesture != GESTURE_TYPE_LEFT_TO_RIGHT)
                state = S_LOCK_NO_GESTURE; 

        break;
        case S_LOCK_INCREMENT_DIGIT:
            if (gesture == GESTURE_TYPE_SHINE)
            {
                state = S_LOCK_CHECK_CODE;
                emit attemptUnlock();
            } else if (gesture != GESTURE_TYPE_DOWN_TO_UP) {
                state = S_LOCK_NO_GESTURE;
            }

        break;
        case S_LOCK_DECREMENT_DIGIT:
            if (gesture == GESTURE_TYPE_SHINE)
            {
                state = S_LOCK_CHECK_CODE;
                emit attemptUnlock();
            } else if (gesture != GESTURE_TYPE_UP_TO_DOWN)
            {
                state = S_LOCK_NO_GESTURE;
            }

        break;
        case S_LOCK_CHECK_CODE:
            if (gesture != GESTURE_TYPE_SHINE)
            {
                state = S_LOCK_NO_GESTURE;
            }
        break;
        default:
            qDebug() << "Unexpected state: " << state << "\n";

    }

    //If the state changes, we should treat this as user activity, which prevents idle-mode in the main state machine
    if (state != last_state)
    {
        lastStateChangeTimeMsec = QDateTime::currentMSecsSinceEpoch();
    }
}