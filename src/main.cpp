/*
* Copyright (C) 2022 Texas Instruments Incorporated - http://www.ti.com/
*
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
#include <QGuiApplication>
#include <QQmlEngine>
#include <qqml.h>
#include <QQmlComponent>
#include <QtSerialPort/QSerialPort>
#include <QDebug>
#include <QQmlContext>
#include <QScreen>

#include "keyword.h"
#include "serialreaderthread.h"
#include "hmi_logic.h"
#include "id_interface.h"
#include "face_image_provider.h"
#include "face_detection.h"
#include "door_control.h"


int main(int argc, char *argv[])
{
    qDebug() << "Open main";

    FaceImageProvider* faceImageProvider;
    QScreen *screen;
    QRect screenGeometry;
    FaceDetectFilter* faceDetectFilter;
    QObject* faceImageQMLObj;
    Keyword* keyword;
    IDInterface* idinterface;
    SerialReaderThread *gestureReaderThread;
    HMILogic* hmiLogic;    
    DoorControl* doorControl;
    int screen_height, screen_width;
    char light_addr_ip[16];
    int light_addr_port;

    /* Register types that are needed in QML files or for signal-slot communications */
    qmlRegisterType<FaceDetectFilter>("com.tihmi.classes", 1, 0, "CvDetectFilter");
    qRegisterMetaType<Gesture_gesture_type>();

    /* Setup the base elements of the application, including loading in the QML file describing the GUI */
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    QQmlEngine engine;
    QQmlComponent component(&engine, QUrl("qrc:/ui/main.qml"));
    QObject *object = component.create();

    printf("Usage: mmwavegesture-hmi -platform eglfs\n");
    if(object == NULL){
        qDebug() << "Cannot create root object\n";
        qDebug() << component.errors();
        return -1;
    }


    /* Set the main window property like image name, displayed width and height of the image */
    screen = QGuiApplication::primaryScreen();
    screenGeometry = screen->geometry();
    screen_width = screenGeometry.width();
    screen_height = screenGeometry.height();
    object->setProperty("width", screen_width);
    object->setProperty("height", screen_height);
    object->setProperty("lockgifsource", "../assets/lock_unlock.gif");

    /* Retrieve objects from the GUI so their elements can be modified in C-land */
    faceDetectFilter = object->findChild<FaceDetectFilter*>("faceDetectFilter");
    faceImageQMLObj = object->findChild<QObject*>("DetectedFace");
    faceImageProvider = new FaceImageProvider(object);

    if (faceImageQMLObj && faceImageProvider) {   
        int face_image_height, face_image_width;
 
        /* Retrieve the height and width of the image that will show detected faces and provide this to the face-detection code so it knows how to resize images */
        face_image_height = faceImageQMLObj->property("height").toInt();
        face_image_width = faceImageQMLObj->property("width").toInt();

        faceImageProvider->setImageWidth(face_image_width);
        faceImageProvider->setImageHeight(face_image_height);
        faceDetectFilter->setImageProvider(faceImageProvider);
        
        // Image provider will populate images in the GUI contained by faceImageQMLObj
        engine.addImageProvider(QLatin1String("faceImageProvider"), faceImageProvider);
    } else {
        qDebug() << "Failed to create faceImageProvider and/or retrieve QML element for showing face-detections";
        return -2;
    }


    /* Create objects that interact with the GUI and inputs at an application level. Those that interact with the GUI use the main object for modifying GUI parameters/elements */
    qDebug() << "Creating objects and QT connections";
    
/* Read information from the command line */
    gestureReaderThread = new SerialReaderThread();
    strncpy(light_addr_ip, NULL_IP_ADDR, strnlen(NULL_IP_ADDR, sizeof(light_addr_ip)));
    light_addr_port = NULL_PORT;

    if (argc >= 2)
    {
        int current_arg = 1;
        while (current_arg < argc)
        {
            if ( std::strncmp(argv[current_arg], "-s", 2) == 0 && current_arg+1 < argc) {
                //get serial port name
                gestureReaderThread->setSerialPortName(argv[current_arg+1]);
                current_arg+=2;
            } else if (std::strncmp(argv[current_arg], "-i", 2) == 0  && current_arg+1 < argc) {
                //get ip and port
                char* addr = argv[current_arg+1];
                char port_str[8];
                char* colon_loc = strchr(addr, (int) ':');
                int colon_pos = (int)(colon_loc - addr);
                if (colon_loc != NULL && colon_pos < strnlen(addr, sizeof(light_addr_ip)) && colon_pos >= 0)
                {
                    memset(light_addr_ip, 0, sizeof(light_addr_ip));
                    memset(port_str, 0, sizeof(port_str));

                    strncpy(light_addr_ip, addr, colon_pos);
                    strncpy(port_str, colon_loc+1,  strlen(addr)-colon_pos-1);
                    light_addr_port = atoi(port_str);
                    current_arg+=2;
                }
                else {
                    current_arg++;
                }
            }
            else if (std::strncmp(argv[current_arg], "-h", 2) == 0 ) {
                qDebug() << "Usage: mmwavegesture_hmi -s /path/to/serial/port -i ip:port";
                if (argc == 2) exit(1);
            }

            else {
                current_arg++;
            }
        }
    }
    
    /* Create objects that interact with the GUI and inputs at an application level. Those that interact with the GUI use the main object for modifying GUI parameters/elements */
    qDebug() << "Creating QT objects";
    keyword = new Keyword(object);
    idinterface = new IDInterface(object);
    hmiLogic = new HMILogic();
    doorControl = new DoorControl(light_addr_ip, light_addr_port);

    qDebug() << "Creating QT Object connections";
    /*Connect the signals and slots sensor-reading threads, HMI logic thread, and GUI-controlling Keyword object */
    //Read activity over serial from mmWave sensor to HMI logic
    QObject::connect(gestureReaderThread, &SerialReaderThread::detectGesture, hmiLogic, &HMILogic::handleGesture, Qt::QueuedConnection);
    QObject::connect(gestureReaderThread, &SerialReaderThread::detectProximity, hmiLogic, &HMILogic::handleProximity, Qt::QueuedConnection );

    //Translate and filter gestures within HMI logic to actions for the combination lock/keyword
    QObject::connect(hmiLogic, &HMILogic::decrementDigit, keyword, &Keyword::handleDownGesture, Qt::QueuedConnection );
    QObject::connect(hmiLogic, &HMILogic::incrementDigit, keyword, &Keyword::handleUpGesture, Qt::QueuedConnection );
    QObject::connect(hmiLogic, &HMILogic::shiftDigitLeft, keyword, &Keyword::handleLeftGesture, Qt::QueuedConnection );
    QObject::connect(hmiLogic, &HMILogic::shiftDigitRight, keyword, &Keyword::handleRightGesture, Qt::QueuedConnection );
    QObject::connect(hmiLogic, &HMILogic::wakeup, keyword, &Keyword::handleMotionDetected, Qt::QueuedConnection );
    QObject::connect(hmiLogic, &HMILogic::idle, keyword, &Keyword::handleNoActivity, Qt::QueuedConnection );

    // Light control from the HMI logic
    if (doorControl->isUsingDoorLight()) {
        QObject::connect(hmiLogic, &HMILogic::idle, doorControl, &DoorControl::doorLightIdle, Qt::QueuedConnection );
        QObject::connect(hmiLogic, &HMILogic::readyToUnlock, doorControl, &DoorControl::doorLightUnlocked, Qt::QueuedConnection );
        QObject::connect(hmiLogic, &HMILogic::doorLock, doorControl, &DoorControl::doorLightLocked, Qt::QueuedConnection );
        QObject::connect(hmiLogic, &HMILogic::doorAwaitVisitor, doorControl, &DoorControl::doorLightAwaitingVisitor, Qt::QueuedConnection );
        QObject::connect(hmiLogic, &HMILogic::wakeup, doorControl, &DoorControl::doorLightLocked, Qt::QueuedConnection );
    }

    //Communicate lock state between GUI, HMI-logic, and face-detection elements
    QObject::connect(hmiLogic, &HMILogic::attemptUnlock, keyword, &Keyword::handleUnlockAttempt, Qt::QueuedConnection );
    QObject::connect(keyword, &Keyword::signalIsUnlocked, hmiLogic, &HMILogic::updateLockState, Qt::QueuedConnection);
    QObject::connect(keyword, &Keyword::signalIsUnlocked, faceDetectFilter, &FaceDetectFilter::controlFaceDetection, Qt::QueuedConnection );
    QObject::connect(keyword, &Keyword::signalIsUnlocked, idinterface, &IDInterface::readyToTakeImage, Qt::QueuedConnection );

    //Communciate between camera inputs, HMI logic, and ID card/interface output
    QObject::connect(hmiLogic, &HMILogic::readyForIDCard, idinterface, &IDInterface::createIDCard, Qt::QueuedConnection );
    QObject::connect(hmiLogic, &HMILogic::idle, idinterface, &IDInterface::resetIDInterface, Qt::QueuedConnection );
    QObject::connect(idinterface, &IDInterface::badgeProduced, hmiLogic, &HMILogic::handleUserAdded, Qt::QueuedConnection );
    QObject::connect(faceDetectFilter, &FaceDetectFilter::faceDetected, idinterface, &IDInterface::createIDCard, Qt::QueuedConnection );
    QObject::connect(idinterface, &IDInterface::finishedCreatingID, faceDetectFilter, &FaceDetectFilter::controlFaceDetection, Qt::QueuedConnection );

    //Reset interface
    QObject::connect(hmiLogic, &HMILogic::resetInterface, keyword, &Keyword::resetLockInterface);
    QObject::connect(hmiLogic, &HMILogic::resetInterface, idinterface, &IDInterface::resetIDInterface);
    

    //Cleanup
    QObject::connect(keyword, &Keyword::keywordQuit, gestureReaderThread, &SerialReaderThread::handleQuit, Qt::QueuedConnection);
    QObject::connect(gestureReaderThread, &SerialReaderThread::srtQuit, &app, &QGuiApplication::quit, Qt::QueuedConnection);

    /* Start thread that reads the gesture from mmWave Sensor and take predetermined action per the gestures received */
    gestureReaderThread->start();

    qDebug() << "Starting application";

    app.exec();

    qDebug() << "QGuiApplication execution over\n";

    return 0;
}

