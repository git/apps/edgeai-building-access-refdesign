/*
* Copyright (C) 2022 Texas Instruments Incorporated - http://www.ti.com/
*
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
#include <QCoreApplication>
#include <QDebug>
#include <QTimer>
#include "keyword.h"

QT_USE_NAMESPACE

const int KW_NUM_VALUES = 10; //Digit.xml has a PathView object with a sequence of numbers whose length must reflect this.
const char* DIGIT_ACTIVATED_BORDER_COLOR = "red";
const int DIGIT_ACTIVATED_BORDER_WIDTH = 10;
const char* DIGIT_DEACTIVATED_BORDER_COLOR = "black";
const int DIGIT_DEACTIVATED_BORDER_WIDTH = 1;

/**
 * @brief Construct a new Keyword:: Keyword object. This will retrieve references to the digit-objects in the main window. By default, it will set the window to be inactive such that nothing is shown on the screen. 
 * 
 * @param qmlCompObject The QObject representing the main body of the QML window within which the lock (combintation digits and lock symbol) are displayed.
 */
Keyword::Keyword(QObject *qmlCompObject)
{
    isIdle = false;

    m_digitArray[0] = qmlCompObject->findChild<QObject *>("myDigit1");
    m_digitArray[1] = qmlCompObject->findChild<QObject *>("myDigit2");
    m_digitArray[2] = qmlCompObject->findChild<QObject *>("myDigit3");
    m_digitArray[3] = qmlCompObject->findChild<QObject *>("myDigit4");

    m_qmlCompObject = qmlCompObject;


    resetLockInterface();
    handleNoActivity();
}

/**
 * @brief Destroy the Keyword:: Keyword object. Removes references to the digits shown in the GUI
 * 
 */
Keyword::~Keyword()
{
    for (int i = 0; i < KW_NUM_DIGITS ; i++){
        m_digitArray[i] = nullptr;
        m_counter[i] = 0;
    }
}

/**
 * @brief emits a signal to report end of application
 * 
 */
void Keyword::emitKeywordQuit()
{
    qDebug() << "Emit keywordQuit signal\n";
    emit keywordQuit();
}

/**
 * @brief Reset the state of the lock's digits to their original state
 * 
 */
void Keyword::resetLockInterface()
{

    //initialize the displayed digits as 5263
    m_counter[0] = 5;
    m_counter[1] = 2;
    m_counter[2] = 5;
    m_counter[3] = 4;

    

    /* Substract the keyword with 3 as the keyword is matched to the "model" index and not
     * the value at that index. Check the PathView in Digit.qml to understand model. A PathView
     * displays data from models created from built-in QML types.  A model provides a
     * set of data that is used to create the items for the view.  To avoid many scrolls to
     * to set the password, password is restricted between 3 and 8 and mmWave part number is
     * 6254 (product number), which is set as initial password*/
    m_keyword[0] = 6;
    m_keyword[1] = 2;
    m_keyword[2] = 5; 
    m_keyword[3] = 4; 

    qDebug() <<  " keyword index" << m_keyword[0] << m_keyword[1] << m_keyword[2] << m_keyword[3];


    //Set the active index to the leftmost one to start
    m_activeIndex = 0;

    //show the active index by giving it a larger border with noticeable color
    m_digitArray[m_activeIndex]->setProperty("borderColor",DIGIT_ACTIVATED_BORDER_COLOR);
    m_digitArray[m_activeIndex]->setProperty("borderWidth", DIGIT_ACTIVATED_BORDER_WIDTH);
    m_digitArray[m_activeIndex]->setProperty("digit", m_counter[0]);

    //Set all the other active indices to be a nondescript border
    for (int i = 1; i < KW_NUM_DIGITS; i++){
        if(m_digitArray[i]){
            m_digitArray[i]->setProperty("borderColor", DIGIT_DEACTIVATED_BORDER_COLOR);
            m_digitArray[i]->setProperty("borderWidth", DIGIT_DEACTIVATED_BORDER_WIDTH);
            m_digitArray[i]->setProperty("digit", m_counter[i]);
        }
    }

    // Depending on whether the interface is supposed to be in an idle state or not, directly call the slot that modifies the interface based on the user's apparent proximity
    if (isIdle)
    {
        handleNoActivity();
    }
    else{
        handleMotionDetected();
    }

    // Report that the lock is now locked
    emit signalIsUnlocked(false);
}

/**
 * @brief Slot to handle a gesture motioning upward. This should increment the digit (with rollover from 9 -> 0)
 * 
 */
void Keyword::handleUpGesture()
{
    //increment digit
    m_counter[m_activeIndex] = (m_counter[m_activeIndex] + 1) % KW_NUM_VALUES;

    m_digitArray[m_activeIndex]->setProperty("digit", m_counter[m_activeIndex]);

    //Refresh the screen with updated digit
    QTimer::singleShot(0, m_qmlCompObject, SLOT(update()));
}

/**
 * @brief @brief Slot to handle a gesture motioning downward. This should decrement the digit (with rollover from 0 -> 9)
 * 
 */
void Keyword::handleDownGesture()
{
    //decrement
    m_counter[m_activeIndex] = (m_counter[m_activeIndex] - 1 + KW_NUM_VALUES) % KW_NUM_VALUES;

    m_digitArray[m_activeIndex]->setProperty("digit", m_counter[m_activeIndex]);

    //Refresh the screen with updated digit
    QTimer::singleShot(0, m_qmlCompObject, SLOT(update()));
    
}

/**
 * @brief Slot to handle a gesture motioning to the left. This should shift the active index to the left (or wrap around if already leftmost)
 * 
 */
void Keyword::handleLeftGesture()
{
    // revert the active index's border to be nondescript
    m_digitArray[m_activeIndex]->setProperty("borderColor", DIGIT_DEACTIVATED_BORDER_COLOR);
    m_digitArray[m_activeIndex]->setProperty("borderWidth", DIGIT_DEACTIVATED_BORDER_WIDTH);

    m_activeIndex = (m_activeIndex - 1 + KW_NUM_DIGITS) % KW_NUM_DIGITS;

    // make new active index have a more noticeable border
    m_digitArray[m_activeIndex]->setProperty("borderColor", DIGIT_ACTIVATED_BORDER_COLOR);
    m_digitArray[m_activeIndex]->setProperty("borderWidth", DIGIT_ACTIVATED_BORDER_WIDTH);

    //Refresh the screen with updated digit index
    QTimer::singleShot(0, m_qmlCompObject, SLOT(update()));
}

/**
 * @brief Slot to handle a gesture motioning to the right. This should shift the active index to the right (or wrap around if already rightmost)
 * 
 */
void Keyword::handleRightGesture()
{
    // revert the active index's border to be nondescript
    m_digitArray[m_activeIndex]->setProperty("borderColor", DIGIT_DEACTIVATED_BORDER_COLOR);
    m_digitArray[m_activeIndex]->setProperty("borderWidth", DIGIT_DEACTIVATED_BORDER_WIDTH);

    m_activeIndex = (m_activeIndex + 1) % KW_NUM_DIGITS;

    // make new active index have a more noticeable border
    m_digitArray[m_activeIndex]->setProperty("borderColor", DIGIT_ACTIVATED_BORDER_COLOR);
    m_digitArray[m_activeIndex]->setProperty("borderWidth", DIGIT_ACTIVATED_BORDER_WIDTH);


    //Refresh the screen with updated digit index
    QTimer::singleShot(0, m_qmlCompObject, SLOT(update()));
}

/**
 * @brief Slot to respond to user's motion. This will transition from an 'idle' state (if it was previously idle) to active, turning the screen from black with no elements visible to grey with elements visible.
 * 
 */
void Keyword::handleMotionDetected()
{
    m_qmlCompObject->setProperty("color", "gray");  //background becomes gray
    m_qmlCompObject->setProperty("activescreen", true); //elements become visible

    if (isIdle != false)
    {
        //if we were idle, wake up and set the lock to active
        qDebug() << "Switch to active state\n";
        isIdle = false;
    }
}

/**
 * @brief Slot to respond to inactivity. This will transition from an 'active' state (if it was previously active) to idle, turning the screen from grey with elements visible to black with no elements visible .
 * 
 */
void Keyword::handleNoActivity()
{
    m_qmlCompObject->setProperty("color", "black");
    m_qmlCompObject->setProperty("activescreen", false);

    isIdle = true;
}


/**
 * @brief Handle an unlocking attempt by checking the digits' values against the keyword/password. If the code is correct, it will produce a signal (signalIsUnlocked) indicating the new state, and it will unpause the lock-gif to depict that the combination was correct.
 * 
 */
void Keyword::handleUnlockAttempt()
{
    bool keyMatch = false;

    keyMatch = ((m_digitArray[0]->property("digit") == m_keyword[0]) &&
            (m_digitArray[1]->property("digit") == m_keyword[1]) &&
            (m_digitArray[2]->property("digit") == m_keyword[2]) &&
            (m_digitArray[3]->property("digit") == m_keyword[3]));

    qDebug() << "Keyword::handleUnlockAttempt: keyMatch=" << keyMatch; 

    if (keyMatch == true){
        qDebug() << "emit unlocked= " << keyMatch;
        emit signalIsUnlocked(keyMatch);

        m_qmlCompObject->setProperty("lockAnimationPaused", false);

    }
}

