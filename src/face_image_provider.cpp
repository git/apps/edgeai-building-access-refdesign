/*
* Copyright (C) 2022 Texas Instruments Incorporated - http://www.ti.com/
*
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#include "face_image_provider.h"

//two psuedo-paths to toggle between to force the image provider to call RequestImage
const char* SOURCE1 = "image://faceImageProvider/id_image1";
const char* SOURCE2 = "image://faceImageProvider/id_image2";

/**
 * @brief This function is called by QT when the Image QML object thinks it needs to update the image. One of these triggers is a change to the image source.
 *      
 *      None of the provided input parameters are being used in this function. It instead returns the image held as an instance variable.
 * 
 *
 * @return QImage The image to display
 */
QImage FaceImageProvider::requestImage(const QString &id, QSize *size, const QSize &requestedSize)  {
    Q_UNUSED(id); Q_UNUSED(size); Q_UNUSED(requestedSize);
    // qDebug() << "Req size: " << requestedSize << "; Image: "<< *m_image;
    return *m_image;
}

/**
 * @brief Update the image held as an instance variable. Calling this will indirectly invoke RequestImage by changing the source such that the supplied image can be provided then. 
 *  
 * @param img The image to use when RequestImage is called. The image should not be deleted or freed in the mean time; this function will handle freeing the image when it is finished with it.
 */
void FaceImageProvider::updateImage(QImage *img)
{
    QImage* old_img = m_image;
    m_image = img;
    delete old_img;

    // Hack to force QImageProvider to update the image -- changing the source will invoke requestImage(), which cannot be directly called. The 'cache' parameter of the QML Image much be 'false'.
    if (source_toggle)
    {
        m_object->setProperty("facedetectedimagesource", SOURCE1);
    }
    else 
    {
        m_object->setProperty("facedetectedimagesource", SOURCE2);
    }
    source_toggle = !source_toggle;
}

/**
 * @brief Set the width of the image that should be provided through this class 
 * 
 */ 
void FaceImageProvider::setImageWidth(int width){
    face_image_width = width;
}

/**
 * @brief Get the width of the image that should be provided through this class
 * 
 * @return int 
 */
int FaceImageProvider::getImageWidth(){
    return face_image_width;
}

/**
 * @brief Set the height of the image that should be provided through this class
 * 
 */
void FaceImageProvider::setImageHeight(int height){
    face_image_height = height;
}

/**
 * @brief Get the height of the image that should be provided through this class
 * 
 * @return int 
 */
int FaceImageProvider::getImageHeight() {
    return face_image_height;
}