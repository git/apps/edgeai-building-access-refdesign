# EdgeAI Building Access Reference Design

The EdgeAI Building Access Reference Design combines vision and radar inputs to create a multimodal, touchless entry system that requires no keys or RFID tags. As shown in the video, a user approaches the system and enters a code using swiping gestures in front of the industrial mmWave Radar sensor (IWR6843-AOPEVM), which classifies gestures using a neural network running in the on-board DSP. The AM62X collects the output gestures and modifies the graphical interface accordingly, and when the code is input correctly, activates the camera to collect a picture for a temporary visitor badge. 

The software stack in this demo features Qt for graphics, gstreamer for image capture (indirectly through Qt), and OpenCV for image processing.


## Additional Components

This reference design requires several hardware components alongside the [(AM62 Starter Kit (SK-AM62 EVM))](https://www.ti.com/tool/SK-AM62) and the materials required in the [quick start guide](https://dev.ti.com/tirex/explore/node?node=ANr.36GYqYm2iRhWSygMYQ__FUz-xrs__LATEST): 

* USB camera capable of taking 1280 x 720 images. This demo used a Logitech C270. Future SDK releases will enable CSI cameras
* mmWave Radar EVM ([IWR6843-AOPEVM](https://www.ti.com/tool/IWR6843AOPEVM))
  * (optional) mmWave Booster Pack board [MMWAVEICBOOST](https://www.ti.com/tool/MMWAVEICBOOST))
  * USB-micro to USB-A cable for serial connection
* Monitor capable of 1920 x 1080 resolution with HDMI input
* A host machine running Linux (preferably an Ubuntu LTS distribution, 18.04 or newer).
* 1 USB-micro to USB-A cable for serial connection to EVMs

NB: A USB-C to >=1 USB-A hub/splitter is necessary unless the camera or serial cable for mmWave is USB-C. The AM62X EVM only has one USB-A port.

![demo hardware block diagram](doc/blockdiagram.png)


## System Setup

Start by setting up the AM62X (with Linux), our local build environment, and the mmwave sensor. 

Please find the latest [Linux Software Development Kit Resources for AM62](https://www.ti.com/tool/download/PROCESSOR-SDK-LINUX-AM62X) on TI.com for most authoritative versions. The SDK version at the time of this demo release is 08.03.00.19.

The [TI Resource Explorer](https://dev.ti.com/tirex/explore) also contains many helpful guides for AM62, such as the [Linux Academy for AM62X](https://dev.ti.com/tirex/explore/node?node=AO2IbV53aUarnRvmyK9fxQ__XaWts8R__LATEST) (See "Evaluating Linux and Develop Linux on TI EVM"), and the mmWave Radar (See "mmWave Sensors -> Industrial Toolbox").


### SK-AM62 Linux Image Setup
--------------------------

The full set of instructions can be found in the [Resource Explorer's AM62x Start Kit EVM Quick Start Guide](https://dev.ti.com/tirex/explore/node?node=ANr.36GYqYm2iRhWSygMYQ__FUz-xrs__LATEST).

*  Download the SD card image from the [AM62 SDK downloads page](https://www.ti.com/tool/download/PROCESSOR-SDK-LINUX-AM62X) for the latest SDK version. This will be "tisdk-default-image-am62xx-evm.wic.xz".
*  Flash to an SD card (8GB or larger) using a tool like desktop-application Balena etcher, command-line application "bmaptool", etc..
*  Insert into the SD slot of the EVM
*  Plug in the power cable, then a USB-micro cable for serial
*  Open a serial terminal (e.g., PuTTY, minicom, CoolTerm) on the host machine for the first enumerated serial port of the 4 that appears when plugged in; use serial settings 115200 Baud 8N1, no hardware flow control.
*  Login with username 'root' and no password

The serial session should now provide access to a linux terminal.

Alternatively, plug in a keyboard and mouse to the EVM and use the onboard command line through the window manager, Weston.

An ethernet connection through a switch or router is a more convenient alternative to serial once IP address is known. Files can be transferred through SCP rather than removing the SD card and copying them using an SD card reader/writer on the host machine. 



### Host Machine SDK Setup


To build applications for the AM62, set up the SDK on a x86 /host machine running Linux. Download a setup the installation script for the SDK from the [AM62 SDK downloads page](https://www.ti.com/tool/download/PROCESSOR-SDK-LINUX-AM62X), the version-dependent file ending with "-Linux-x86-Install.bin". Ensure the version of this matches the WIC image flashed to the SD card. When run, the installation script will ask where to install. 

Once complete, open a terminal and navigate to the installation directory. To build an application for SK-AM62, set up the environment by running: 

```
cd arago/linux-devkit
source ./environment-setup
```


### mmWave Radar Setup


For this demo, the Industrial mmWave Radar Sensor is treated as a black box. An existing TI Reference Design for the 9 Gesture Demo will perform gesture recognition by running an Artificial Neural Network on radar data using the mmWave Radar's on-package DSP.

To flash the device, install [Uniflash](https://www.ti.com/tool/UNIFLASH).

Follow instructions on the [Gesture With Machine Learning](https://dev.ti.com/tirex/explore/node?a=VLyFKFf__4.12.1&node=A__AB3P8Iq.cVgCtrFYFhvt7Q__com.ti.mmwave_industrial_toolbox__VLyFKFf__LATEST) to load this firmware onto your mmWave AOP sensor.

Once the board is flashed and the boot pins are set back into functional mode, continue to the next step in this readme.


## Building the Building Access Application


After *source*ing the environment, the current input line in the terminal will be preceded by `[linux-devkit]`. 


### Workaround for SDK version 08.03.00.19


If the SDK version is 08.03.00.19, then the mmwavegesture_hmi.pro file needs to be modified to point to a separate path containing the /usr directory that will be on the root filesystem of the AM62X. The sysroot is out of sync with the generated filesystem, and will otherwise through linker errors for libraries like OpenCV. 

*  Navigate to the SDK installation's `bin` folder
*  Execute `setup-targetfs-nfs.sh`. If the printout says Multiple Filesystems Found, select the one including "ti-default-image". This creates the filesystem that is identical to what the WIC image puts onto the SD card
*  Copy the path to this filesystem's "usr" directory", and add to the mmwavegesture_hmi.pro file to replace the value for `usr_path = /path/to/sysroot/or/rootfs/usr/` with the path to your own


### Making the Demo Binary

Make the demo binary within the root directory of the cloned repo:


```    
qmake -config release
make
```


## Running the Demo


The system is now ready to run the demo. The next steps are to connect extra hardware components, start the, and step through its stages.

### Connecting and Positioning Components


First connect the EVM as shown. Power, serial, HDMI, and perhaps ethernet should already be connected. The USB camera and serial-USB cable (to the mmWave sensor) should be plugged in while the EVM is running. 

The E2 EVM only has 1 USB-A port, so an adapter or hub should be used. Monitor ``dmesg`` if this is problematic. Best results with early versions of the Processor SDK were when the hub was plugged in before booting and when the USB devices plugged in through the hub/adapter are connected after boot.

![AM62X-SK E2EVM](doc/AM62X-SK-E2.png)

The camera should be mounted near eye level. Face detection is most effective when looking at the camera head-on at a neutral angle. 

The mmWave sensor should be set up near elbow level. In the image shown below, the IWR6843AOP-EVM is attached to a MMWAVEICBOOST board. This is optional, but provides slightly better performance since it was present during all training of the gesture-recognition neural network. The AOP EVM and the IC Boost board should each come with a mounting hole that matches a standard camera mounting screw. 

![AOP ICBoost Mounted](doc/mmWave-AOP-ICBoost.png)

### Starting the Demo


To run the demo, copy it over to the SK-AM62 board. The assets/ and ui/ directories should also be copied over, as they contain important resources files. It is recommended to tarball the root directory. 

*  `tar -cf edgeai_buildingaccess_demo.tar /path/to/demo/repo/*`
*  Transfer tarball to AM62 (copy to SD card or scp over network)
*  `tar -xf edgeai_buildingaccess_demo.tar`

The demo will assume that the video feed is available through /dev/video0 and the mmWave Radar through /dev/ttyACM1. If the mmWave Radar is not using a MMWAVEICBOOST booster board, then the device name will likely be different - typically /dev/ttyUSB1.

Stop the window compositor, weston:

`systemctl stop weston`

Run the demo as follows from the root directory of the untarballed and built repo:

`./build/release/mmwavegesture_hmi -platform eglfs -s /dev/MMWAVE_DEVICE_NAME`

### Interacting with the Demo


This open source version of the building access demo has four phases: wakeup, unlock, create ID card, and entry.

When the demo first boots, then screen will be black to simulate the system being in a low-power idle state. Making any gesture in front of the mmWave Radar sensor will wake up the demo. The screen should look similar to the one below:

![locked interface](doc/interface-lockscreen.png)

At this point, the system is waiting for the user to put in a lock code. The directions on the screen describe how to do so with swiping gestures in front of the mmWave AOP EVM (the large black component on the EVM is the radar module). Performing the gestures within 10cm (4 inches) is strongly recommended, although they can be detected up to 40cm (~1.25 ft) away.

Note that gesture recognition is not flawless -- it is running a neural network, and some ways of performing the gestures are more effective than others based on the original training data. Also note that since these are directional gestures, changing the orientation (pitch or roll the EVM) will mean the wrong gestures are detected.

Once the code is input, the lock will show that it is unlocking. If it does not react, try the manual unlock gesture (shine), which is like miming the act of throwing sand.

In the next phase, the camera will turn on within the left portion of the screen and perform face-detection. When a face is detected, it will highlight it with a white box around the user's face. Once the camera has been on for 5 seconds, it will use the next face-image it finds as the picture for an ID card, so get in position and smile! It will show you the image like below:

![face detected and ID provided](doc/interface-face-detect.png)

The building is now "unlocked"! The new visitor is given 15 seconds before it relocks and resets to the lock screen. After around a minute of inactivity, it will go back into its idle state.

