/*
* Copyright (C) 2022 Texas Instruments Incorporated - http://www.ti.com/
*
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

import QtQuick 2.14
import QtQuick.Window 2.14
import QtMultimedia 5.14

import com.tihmi.classes 1.0

Window{
    id:mainWindow
    objectName: "mainWindow"
    visible: true
    width: 1920
    height: 1080
    title: qsTr("mmWave + Sitara Gesture Control Demo")
    color: "black"
    property string lockgifsource
    property string facedetectedimagesource
    property bool lockAnimationPaused : true
    property bool activescreen: false
    property bool isfacedetected : false
    property bool lockUnlocked: false
    property bool showcamera: false

    property real lockUILeftAnchor: mainWindow.width/2
    property real lockUIRightAnchor: mainWindow.width
    property real lockUIWidth: mainWindow.width/2
    property real lockUIHeight: mainWindow.height

    property real faceDetectUILeftAnchor: 0
    property real faceDetectUIRightAnchor: lockUILeftAnchor
    property real faceDetectUIWidth: mainWindow.width/2
    property real faceDetectUIHeight: mainWindow.height

    FaceDetectUI {
        id: faceDetectGUI
        objectName: "faceDetectGUI"
        height: faceDetectUIHeight
        width: faceDetectUIWidth

        visible: activescreen

        isfacedetected: mainWindow.isfacedetected
        showcamera: mainWindow.showcamera
    }


    /** Separators between GUI elements **/
    Rectangle {
        visible: mainWindow.activescreen
        height: mainWindow.height * 0.915
        width: 4
        x: lockUIWidth - width/2
        y: mainWindow.height * 0.085
        color: "black"
    }

    Rectangle {
        visible: mainWindow.activescreen
        height: 4//mainWindow.height * 0.915
        width: lockUIWidth
        x: lockUIWidth
        y: lockUIHeight * 0.38
        color: "black"
    }

    Rectangle {
        visible: mainWindow.activescreen
        height: 4//mainWindow.height * 0.915
        width: mainWindow.width
        x: 0
        anchors.top: title.bottom
        color: "black"
    }
    

    /* Title and Directions */
    Text {
        visible: activescreen
        id: title
        objectName: "title"
        text: "Texas Instruments AM62X EdgeAI Demo"
        font.family: "Helvetica"
        font.pointSize: 64
        font.bold: true
        fontSizeMode: Text.Fit
        width: mainWindow.width*0.6
        height: mainWindow.height*0.0675
        wrapMode: Text.Wrap
        color: "white"
        x: mainWindow.width/2 - width/2
        y: faceDetectUIHeight*0.015
    }

    Text {
        visible: activescreen
        id: demoInstructions
        objectName: "demoInstructions"
        text: "Demo Instructions"
        font.family: "Helvetica"
        font.pointSize: 48
        font.underline: true
        fontSizeMode: Text.Fit
        width: lockUIWidth * 0.9
        height: lockUIHeight * 0.0675
        wrapMode: Text.Wrap
        color: "white"
        x: lockUILeftAnchor + lockUIWidth/2 - width/2
        y: faceDetectUIHeight*0.09
    }
    
    Text {
        visible: activescreen
        objectName: "lockUIText"
        text: "Visitors enter Passcode (6254) with gestures:" + 
                "\n  - Perform hand gestures within 10cm of radar module. " +
                "\n  - Swipe up or down for +1/-1 to highlighted digit. " +
                "\n  - Swipe left or right to move the digit index." + 
                "\nPlace your face in oval for face detection/recognition. "
        font.family: "Helvetica"
        font.pointSize: 48
        fontSizeMode: Text.Fit
        width: lockUIWidth*0.9
        height: lockUIHeight*0.22
        wrapMode: Text.Wrap
        color: "white"
        x: lockUILeftAnchor + lockUIWidth/2 - width/2
        y: faceDetectUIHeight*0.16
    }

    /* Lock UI Elements */
    /** Combination lock assets **/
        Digit { id: digit1; visible: activescreen; objectName: "myDigit1"; x:lockUILeftAnchor + lockUIWidth*0.05; y:lockUIHeight * 0.4; size:lockUIWidth*0.2}
        Digit { id: digit2; visible: activescreen; objectName: "myDigit2"; anchors.left:digit1.right; anchors.leftMargin:0.033*lockUIWidth; y:digit1.y; size:digit1.size}
        Digit { id: digit3; visible: activescreen; objectName: "myDigit3"; anchors.left:digit2.right; anchors.leftMargin:0.033*lockUIWidth; y:digit1.y; size:digit1.size}
        Digit { id: digit4; visible: activescreen; objectName: "myDigit4"; anchors.left:digit3.right; anchors.leftMargin:0.033*lockUIWidth; y:digit1.y; size:digit1.size}

    property int activeid: 0

    AnimatedImage{
        id:lock
        objectName: "lockgif"
        visible: activescreen
        source: mainWindow.lockgifsource

        width:lockUIWidth * 0.6
        height: lockUIHeight * 0.3
        property int idx: 1
        x:(lockUILeftAnchor) + lockUIWidth/2 - width/2
        y: lockUIHeight*0.62// (parent.height-height)/4
    
        speed: 0.6
        paused: mainWindow.lockAnimationPaused
        onFrameChanged: if(currentFrame==frameCount-1 && !lockUnlocked) { 
            mainWindow.lockAnimationPaused = true; 
        }
    }

    Image {
        id: tilogo
        visible: activescreen
        // x: mainWindow.width - 300
        // y: mainWindow.height - 50
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
        anchors.rightMargin: 20
        source: "../assets/TI-logo.png"
    }


}
