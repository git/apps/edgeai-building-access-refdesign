/*
* Copyright (C) 2022 Texas Instruments Incorporated - http://www.ti.com/
*
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

import QtQuick 2.14
import QtMultimedia 5.14

import com.tihmi.classes 1.0


Item {
    // visible: true

    property bool isfacedetected : false
    property bool showcamera: false

    Camera {
        id: camera

        viewfinder.resolution: Qt.size(1280, 720)


        imageProcessing.whiteBalanceMode: CameraImageProcessing.WhiteBalanceFlash

        exposure {
            exposureCompensation: -1.0
            exposureMode: Camera.ExposurePortrait
        }

        flash.mode: Camera.FlashRedEyeReduction

    }

    CvDetectFilter{
        id: faceDetectFilter
        objectName: "faceDetectFilter"
    }

    VideoOutput {
        id: facedetectvideo
        // visible: true
        visible: showcamera
        width:parent.width
        source: camera
        focus : visible // to receive focus and capture key events when visible
        x:0

        filters: [ faceDetectFilter ]
    }


    /** ID card assets **/
    Item {
        id: idcard_holder
        objectName: "IDCard"

        visible: isfacedetected
        // visible: true


        width: parent.width * 0.635
        height: parent.height * 0.3
        x: parent.width * 0.2
        y: parent.height * 0.63

        Rectangle {
            id: card_background
            width: parent.width
            height: parent.height

            border.color: "black"
            border.width: 8
            color: "white"
            radius: 30
        }

        Rectangle {
            id: card_topbottom_seperator
            x:parent.width * .45
            y:parent.height/2
            width: parent.width * 0.525
            height:8

            color:"black"
        }

        Image {
            id:detectedface
            objectName:"DetectedFace"
            
            width: parent.height * 0.74 
            height: parent.height * 0.74 
            
            anchors.horizontalCenter: idcard_holder.horizontalCenter
            anchors.horizontalCenterOffset: -1 * idcard_holder.width * 0.27
            anchors.verticalCenter: idcard_holder.verticalCenter
            source: mainWindow.facedetectedimagesource
            cache: false
        }

        Rectangle {
            id: detectedface_image_border
            width: parent.height * 0.77
            height: parent.height * 0.77

            color: "transparent"
            border.color: "red"
            border.width: 6

            anchors.horizontalCenter: idcard_holder.horizontalCenter
            anchors.horizontalCenterOffset: -1 * idcard_holder.width * 0.27
            anchors.verticalCenter: idcard_holder.verticalCenter
        }

         Rectangle {
            id: visitor_text_container
            objectName: "UserTextContainer"
            x: parent.width * 0.51
            y: parent.height * 0.1
            width: parent.width * 0.4
            height: parent.height * 0.3
            color: "transparent"

            Text {
                id: user_text
                objectName: "UserText"
                text:"TI Visitor Badge"
                color:"red"
                font.family: "Helvetica"
                font.pointSize: 32
                fontSizeMode: Text.Fit
                wrapMode: Text.Wrap
                
                anchors.verticalCenter: parent.verticalCenter 
                horizontalAlignment: Text.AlignHCenter
                anchors.centerIn: parent
                anchors.top: parent.top
            }
         }

        Rectangle {
            id: access_text_container
            x: parent.width * 0.465
            y: parent.height * 0.62
            width: parent.width * 0.5
            height: parent.height * 0.3
            color: "transparent"
            anchors.horizontalCenter: visitor_text_container.horizontalCenter

            Text {
                text:"Access Granted"
                color:"yellowgreen"
                width: parent.width
                height: parent.height
                font.family: "Helvetica"
                font.pointSize: 32
                fontSizeMode: Text.Fit
                wrapMode: Text.Wrap

                anchors.verticalCenter: parent.verticalCenter 
                horizontalAlignment: Text.AlignHCenter
                anchors.centerIn: parent
            }
        }

    }

}