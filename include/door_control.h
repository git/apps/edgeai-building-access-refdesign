/*
* Copyright (C) 2022 Texas Instruments Incorporated - http://www.ti.com/
*
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 * @brief Control a light wirelessly to display the status of the lock. 
 * Red means locked, yellow means awaiting user image, green means unlocked, and white means the system is idle (and assumedly locked).
 * The light is controlled by a SimpleLink WiLink MCU (WL18xx) running a simple TCP server that accepts an incoming connection over wifi and will receive commands as sent from this DoorControl class. 
 * The Wilink MCU will connect, by default, to an SSID SitaraAP_mmwaveDemo with password 'tiwilink8'. It will establish its own IP address and respond to a specific port (by default, 192.168.43.150:6254)
 * 
 */

#ifndef DOOR_CONTROL_H
#define DOOR_CONTROL_H

#include <QThread>
#include <QDebug>

#include <stdio.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>    
#include <inttypes.h>
#include <fcntl.h>
#include <netdb.h>
#include <sys/types.h>


extern const uint16_t WILNK_DEFAULT_PORT;
extern const uint16_t NULL_PORT;
extern const char WILINK_DEFAULT_IP_ADDR[16];
extern const char NULL_IP_ADDR[16];

class DoorControl: public QObject
{
    Q_OBJECT

public: 
    DoorControl(const char ip[16], const int port);
    ~DoorControl(void);
    bool isUsingDoorLight();

signals: 
    void finished();

public slots: 
    void doorLightUnlocked();
    void doorLightLocked();
    void doorLightAwaitingVisitor();
    void doorLightIdle();
    void shutdownDoorLight();


private:
    char wilink_ip[16];
    int WILNK_DEFAULT_PORT;
    bool is_connected, no_connect;
    struct sockaddr_in servaddr;
    int sockfd;

    bool connectToDoor();
    bool isConnected(int sockfd);
    int createSocket();


};

#endif //DOOR_CONTROL_H