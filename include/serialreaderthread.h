/*
* Copyright (C) 2022 Texas Instruments Incorporated - http://www.ti.com/
*
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 * @brief The SerialReaderThread interacts with the serial port to read gesture information sent by a mmWave radar sensor running a neural network within the on-chip DSP. This uses the 9-gesture demo found in the Industrial mmWave Toolbox on ti.com
 * 
 */
#ifndef SERIALREADERTHREAD_H
#define SERIALREADERTHREAD_H

#include <unistd.h>
#include <QThread>
#include <QtSerialPort/QSerialPort>
#include <QDateTime>

#include "keyword.h"
#include "mmwgesturein.h"
#include "math.h"

//The debounce ignores the next N gesture readings to help filter the gesture recognitions reported by the sensor
const int GESTURE_DEBOUNCE_TIMEOUT_COUNT = 10;
const qint64 GESTURE_DEBOUNCE_TIMEOUT_MS = 750;

class SerialReaderThread : public QThread
{
    Q_OBJECT

public:
    SerialReaderThread(bool onlyProximity = false);
    ~SerialReaderThread(void);
    void setSerialPortName(QString port_name);


signals:
    void detectGesture(Gesture_gesture_type gesture);
    void detectProximity(bool motionDetected);
    void srtQuit();

public slots:
    void handleQuit();

protected:
    void interpretGestureData(QByteArray *);
    void interpretProximityData(QByteArray *);
    void run();

private:
    int createSerialPort();
    Gesture_gesture_type selectGesture(mmwave_9_gesture_prob_t values, mmwave_9_gesture_prob_t thresholds);
    mmwave_9_gesture_prob_t formatGestureData(mmwave_9_gesture_uartdata_t *);
    QSerialPort *m_serialPort;
    Gesture_mode_type m_privmode;
    bool m_runSerialThread;
    bool m_onlyProximity;
    QString m_serialPortName;
};
#endif // SERIALREADERTHREAD_H
