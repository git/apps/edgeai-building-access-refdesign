/*
* Copyright (C) 2022 Texas Instruments Incorporated - http://www.ti.com/
*
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
#ifndef MMWGESTUREIN_H
#define MMWGESTUREIN_H

#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <endian.h>

#define MMWAVE_9_GESTURE true
#define MMWAVE_9_GESTURE_MSG_LENGTH 192

#if MMWAVE_9_GESTURE

const char MAGIC_WORD[8] = {2, 1, 4, 3, 6, 5, 8, 7}; 
const int NUM_GESTURES = 10;

typedef enum Gesture_gesture_type_e
{
    GESTURE_TYPE_NONE = 0,
    GESTURE_TYPE_UP_TO_DOWN = 1,
    GESTURE_TYPE_DOWN_TO_UP = 2,
    GESTURE_TYPE_LEFT_TO_RIGHT = 3,
    GESTURE_TYPE_RIGHT_TO_LEFT = 4,
    GESTURE_TYPE_OFF = 5,
    GESTURE_TYPE_ON = 6, 
    GESTURE_TYPE_TWIRL_CW = 7, 
    GESTURE_TYPE_TWIRL_CCW = 8,
    GESTURE_TYPE_SHINE = 9
} Gesture_gesture_type;

Q_DECLARE_METATYPE(Gesture_gesture_type)

typedef enum Gesture_proximity_type_e
{
    PROXIMITY_0_5m = 'A',
    PROXIMITY_1m = 'B',
    PROXIMITY_2m = 'C',
    PROXIMITY_3m = 'D',
    PROXIMITY_NO_MOTION = 'N'
} Gesture_proximity_type;

/* Enum definitions for gesture mode information */
typedef enum Gesture_mode_type_e
{
    GESTURE_MODE_IDLE = 1U,
    GESTURE_MODE_GESTURE = 2
}Gesture_mode_type;

struct header_s
{
    char magicword[8];
    char sdk_version[4];
    char packet_length[4];
    char platform[4];
    char frame_no[4];
    char cpu_time[4];
    char num_detected_obj[4];
    char tlv_no[4];
    char subframe_no[4];
    // char pad[8];

} __attribute__((packed));

struct gesture_features_s   
{
    char tlv_type[4];
    char tlv_len[4];
    char wghtd_doppler[4];
    char wghtd_pos_doppler[4];
    char wghtd_neg_doppler[4];
    char wghtd_range[4];
    char num_thresh_points[4];
    char wghtd_azimuthmean[4];
    char wghtd_azimuth_corr[4];
    char azimuth_doppler_corr[4];
    char wghtd_azimuth_disp[4];
    char wghtd_elevation_disp[4];

} __attribute__((packed));

struct ann_probabilities_s
{
    char tlv_type[4];
    char tlv_len[4];
    char prob_null[4]; //little-endian formatted single-precision floats
    char prob_l2r[4];
    char prob_r2l[4];
    char prob_u2d[4];
    char prob_d2u[4];
    char prob_cw[4];
    char prob_ccw[4];
    char prob_off[4];
    char prob_on[4];
    char prob_shine[4];

} __attribute__((packed)) ;

struct mmwave_9_gesture_uartformat
{
 
    struct header_s hdr;
    struct gesture_features_s gesture_features; 
    struct ann_probabilities_s ann_probabilities;

} __attribute__((packed)) ;

typedef union mmwave_9_gesture_uartdata
{
    char bytearray[192];
    struct mmwave_9_gesture_uartformat gesture_data;
} mmwave_9_gesture_uartdata_t ;

typedef struct mmwave_9_gesture_prob
{
    float prob_null;
    float prob_l2r;
    float prob_r2l;
    float prob_u2d;
    float prob_d2u;
    float prob_cw;
    float prob_ccw;
    float prob_off;
    float prob_on;
    float prob_shine;
} mmwave_9_gesture_prob_t;

const mmwave_9_gesture_prob_t GESTURE_CONFIDENCE_THRESHOLDS = 
{
    0.65, //NONE (Undertrained)
    0.6, //U2D
    0.675, //D2U
    0.6, //L2R
    0.6, //R2L
    1.0, //OFF / NONE (Overtrained, unused)
    1.0, //ON / NONE (Overtrained, unused)
    1.0, //CW (unused)
    1.0, //CCW (unused)
    0.925, //SHINE (check lock combo)
};
#else
/* Bit Field definitions for gesture report */

/* Mode indicator bits */
#define GESTURE_MODE_BIT_SHIFT                  0U
#define GESTURE_MODE_BIT_END                    2U
#define GESTURE_MODE_BIT_MASK                   0x7U

#define GESTURE_ERROR_BIT_SHIFT                 3U
#define GESTURE_ERROR_BIT_END                   3U
#define GESTURE_ERROR_BIT_MASK                  0x8U

#define GESTURE_MODET_BIT_SHIFT                 4U
#define GESTURE_MODET_BIT_END                   4U
#define GESTURE_MODET_BIT_MASK                  0x10U

/* Detected Gesture Type*/
#define GESTURE_TYPE_BIT_SHIFT                  0U
#define GESTURE_TYPE_BIT_END                    7U
#define GESTURE_TYPE_BIT_MASK                   0x7F

/* Enum definitions for gesture mode information */
typedef enum Gesture_mode_type_e
{
    GESTURE_MODE_IDLE = 2,
    GESTURE_MODE_GESTURE = 4
}Gesture_mode_type;

typedef enum Gesture_gesture_type_e
{
    GESTURE_TYPE_NONE = 1U,
    GESTURE_TYPE_UP_TO_DOWN = 2,
    GESTURE_TYPE_DOWN_TO_UP = 4,
    GESTURE_TYPE_LEFT_TO_RIGHT = 8,
    GESTURE_TYPE_RIGHT_TO_LEFT = 16,
    GESTURE_TYPE_CCW_ROTATION = 32, /* Counter ClockWise Rotation */
    GESTURE_TYPE_CW_ROTATION  = 64 /* ClockWise Rotation */
}Gesture_gesture_type;

#endif





#endif /* MMWGESTUREIN_H */

