/*
* Copyright (C) 2022 Texas Instruments Incorporated - http://www.ti.com/
*
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
/**
 * @brief The FaceImageProvider class overloads QQuickImageProvider so that it can provide image to a QML element at runtime. Specifically, this provides images of faces detected to be used in displayed ID card/visitor badges. 
 * 
 *      The QQuickImageProvider will request an image whenever the Image in the QML interface changes its source. The RequestImage method is called by QT when this happens, and provides an opportunity to modify the image shown. Unfortunately, RequestImage cannot be directly invoked, so this class takes advantage of the fact that modifying the source causes this method to be called. 
 * 
 *      For this to work, it is important that that the Image in QML has its 'cache' property set to false, else the image won't actually change.
 *
 * 
 */

#ifndef FACEIMAGEPROVIDERFILTER_H
#define FACEIMAGEPROVIDERFILTER_H

#include <QObject>
#include <QDebug>
#include <QImage>
#include <QQuickImageProvider>

class FaceImageProvider: public QQuickImageProvider
{
    QImage* m_image;
    QObject* m_object;
    bool source_toggle;
	int face_image_width;
	int face_image_height;
    public:
        FaceImageProvider(QObject* object)
            : QQuickImageProvider(QQuickImageProvider::Image)
        {
            m_object = object;
            m_image = new QImage("assets/face.png");
            source_toggle = false;
        }

        FaceImageProvider(QImage *img)
            : QQuickImageProvider(QQuickImageProvider::Image)
        {    
            m_image = img;
        }

        QImage requestImage(const QString &id, QSize *size, const QSize &requestedSize) override;
        void updateImage(QImage *img);

		void setImageWidth(int width);
		int getImageWidth();		
		void setImageHeight(int height);
		int getImageHeight();
};

#endif