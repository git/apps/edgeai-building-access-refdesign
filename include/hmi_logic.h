/*
* Copyright (C) 2022 Texas Instruments Incorporated - http://www.ti.com/
*
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
#ifndef HMI_LOGIC_H
#define HMI_LOGIC_H


#include <QThread>
#include <QDebug>
#include <QTimer>
#include <QDateTime>

#include "mmwgesturein.h"
#include "door_control.h"

const int RESET_TIMER_DELAY_MSEC = 12 * 1000;

typedef enum HMI_super_state
{
    S_IDLE = 0,
    S_LOCK = 10,
    S_ADD_USER = 20,
    S_UNLOCKED = 30
} HMI_super_state_t;

typedef enum HMI_gesture_lock_state
{
    S_LOCK_NO_GESTURE = S_LOCK+1,
    S_LOCK_SHIFT_LEFT = S_LOCK+2,
    S_LOCK_SHIFT_RIGHT = S_LOCK+3,
    S_LOCK_INCREMENT_DIGIT = S_LOCK+4,
    S_LOCK_DECREMENT_DIGIT = S_LOCK+5,
    S_LOCK_CHECK_CODE = S_LOCK+6,
} HMI_gesture_lock_state_t;

typedef enum CameraResult
{
    CAM_NO_FACE_DETECTED = 0,
    CAM_FACE_DETECTED = 1
} CameraResult_t;

class HMILogic : public QObject   
{
    Q_OBJECT

public:
    HMILogic(void);
    ~HMILogic(void);

signals:
    void wakeup();
    void idle();
    void shiftDigitRight();
    void shiftDigitLeft();
    void incrementDigit();
    void decrementDigit();
    void attemptUnlock();
    void readyForIDCard();

    void doorLock();
    void readyToUnlock();
    void doorAwaitVisitor();

    void resetInterface();
    void finished();

public slots:
    void handleQuit();
    void handleProximity(bool motionDetected);
    void handleGesture(Gesture_gesture_type ges);
    void handleUserAdded();
    void updateLockState(bool unlocked);
    void sendReset();

protected:
    void executeFSM(bool motionDetected, Gesture_gesture_type gesture, CameraResult_t cameraResult, bool locked);
    void executeGestureLockFSM(Gesture_gesture_type gesture, bool reset);

private: 
    bool lastPrecenseDetection;
    Gesture_gesture_type lastGestureID; 
    CameraResult_t lastCameraResult; //smart to have enum or type for this describing the application-level implications of what cam saw
    bool isUnlocked;
    qint64 lastStateChangeTimeMsec;

    void externalLock();
    void externalUnlock();

};


#endif /* HMI_LOGIC_H */