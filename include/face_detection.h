/*
* Copyright (C) 2022 Texas Instruments Incorporated - http://www.ti.com/
*
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
#ifndef FACEDETECTION_H
#define FACEDETECTION_H

#include <iostream>

#include <QObject>
#include <QDebug>
#include <QVideoFilterRunnable>
#include <QAbstractVideoFilter>
#include <QImage>
#include <QDateTime>
#include <QQuickImageProvider>

#include "opencv2/opencv.hpp"
#include "opencv2/objdetect.hpp"
#include "opencv2/imgproc.hpp"

#include "face_image_provider.h"

/**
 * @brief The FaceDetectFilterRunnable class is what runs face detection and image processing on video frames. The main 'run' function is called on every new QVideoFrame produced by the video filter (FaceDetectFilter). The output images have a box drawn around the detected face (if face detection is signalled to run), and a cropped image of the face is send to the image provider (FaceImageProvider) so it can be shown in the GUI
 * 
 */
class FaceDetectFilterRunnable : public QVideoFilterRunnable
{
	// Q_OBJECT
private:
	bool doFaceDetection = false;
	qint64 timeStartFaceDetectMsec;
	cv::CascadeClassifier cascade;

	QAbstractVideoFilter* filter;
	FaceImageProvider* faceImageProvider;
public:
	FaceDetectFilterRunnable(QAbstractVideoFilter* creator);
	QVideoFrame run(QVideoFrame* input, const QVideoSurfaceFormat& surfaceFormat, RunFlags flags);

	std::vector<cv::Rect> detectFaces(cv::Mat rgb_image, double scale_x, double scale_y);
	void processDetectedFace(cv::Mat image, cv::Rect face, double scale_x, double scale_y);
	void sendFaceImage(cv::Mat image, cv::Rect face_dims, FaceImageProvider* provider);

	void startDetecting(bool enable);
	bool isDetecting();
	void setImageProvider(FaceImageProvider* fip);
	FaceImageProvider* getFaceImageProvider();

};


/**
 * @brief The FaceDetectFilter class modifies video inputs and produces video outputs frame by frame. This filter is paired with FaceDetectFilterRunnable, which performs face detection on each image and draws a box around each detected face. 
 * 
 * 		This filter is retrievable directly from the GUI, but the runnable within it is not, so it is used as a proxy for getters and setters. All image processing is handled within the attached runnable
 * 
 */
class FaceDetectFilter : public QAbstractVideoFilter
{
	Q_OBJECT

public:
	QVideoFilterRunnable* createFilterRunnable();

	void setRunFaceDetection(bool enable);
	bool isRunningFaceDetect();
	void setImageProvider(FaceImageProvider* fip);
	FaceImageProvider* getFaceImageProvider();

signals:
	void faceDetected();

public slots:
	void controlFaceDetection(bool enable);

private: 
	FaceDetectFilterRunnable* faceDetectFilterRunnable = NULL;
	FaceImageProvider* faceImageProvider = NULL;

};

#endif //FACEDETECTION_H