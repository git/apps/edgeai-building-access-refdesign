/*
* Copyright (C) 2022 Texas Instruments Incorporated - http://www.ti.com/
*
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 * @brief This class controls GUI elements for the lock and combination display. Slots are used through QT to provide inputs from the HMI logic based on gestures recognized via mmWave
 * 
 */
#ifndef KEYWORD_H
#define KEYWORD_H

#include <QByteArray>
#include <QObject>

QT_USE_NAMESPACE
QT_BEGIN_NAMESPACE
QT_END_NAMESPACE

const int KW_NUM_DIGITS = 4;

class Keyword : public QObject
{
    Q_OBJECT

public:
    Keyword(QObject *object);
    ~Keyword();
    unsigned int m_keyword[KW_NUM_DIGITS];

signals:
    void signalIsUnlocked(bool unlocked);
    void keywordQuit();

public slots:
    void handleUpGesture();
    void handleDownGesture();
    void handleLeftGesture();
    void handleRightGesture();
    void handleMotionDetected();
    void handleNoActivity();
    void handleUnlockAttempt();
    void resetLockInterface();
    void emitKeywordQuit();

protected:

private:
    QObject *m_digitArray[KW_NUM_DIGITS];
    unsigned char m_counter[KW_NUM_DIGITS];
    unsigned char m_activeIndex;
    QObject *m_qmlCompObject;
    bool isIdle;
};

#endif
