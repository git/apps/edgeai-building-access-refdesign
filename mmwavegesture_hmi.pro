QT += quick serialport core multimedia

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += ./include

SOURCES += src/main.cpp  src/keyword.cpp src/serialreaderthread.cpp src/hmi_logic.cpp  \
        src/face_image_provider.cpp src/face_detection.cpp  \ 
        src/id_interface.cpp src/door_control.cpp

HEADERS +=  include/keyword.h include/serialreaderthread.h include/mmwgesturein.h  \ 
        include/hmi_logic.h include/face_image_provider.h include/face_detection.h  \ 
        include/id_interface.h include/door_control.h

CONFIG(debug, debug|release) {
        unix: DEFINES += DEBUG
        build_dir = build/debug
} else {
        unix: DEFINES += RELEASE
        build_dir = build/release
}

DESTDIR = $${build_dir}/
OBJECTS_DIR = $${build_dir}/obj
MOC_DIR = $${build_dir}/moc
UI_DIR = ui

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH = ui

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES +=

unix: LIBS += -lopencv_gapi -lopencv_stitching -lopencv_aruco -lopencv_bgsegm -lopencv_bioinspired -lopencv_ccalib -lopencv_dpm -lopencv_face -lopencv_fuzzy -lopencv_hfs -lopencv_img_hash -lopencv_line_descriptor -lopencv_quality -lopencv_reg -lopencv_rgbd -lopencv_saliency -lopencv_sfm -lopencv_stereo -lopencv_structured_light -lopencv_phase_unwrapping -lopencv_superres -lopencv_optflow -lopencv_surface_matching -lopencv_tracking -lopencv_datasets -lopencv_plot -lopencv_videostab -lopencv_video -lopencv_xfeatures2d -lopencv_shape -lopencv_ml -lopencv_ximgproc -lopencv_xobjdetect -lopencv_objdetect -lopencv_calib3d -lopencv_features2d -lopencv_highgui -lopencv_videoio -lopencv_imgcodecs -lopencv_flann -lopencv_xphoto -lopencv_photo -lopencv_imgproc -lopencv_core

#UPDATE!
usr_path = /path/to/sysroot/or/rootfs/usr/

unix: INCLUDEPATH += $${usr_path}/include/opencv4
unix: LIBS += -L$${usr_path}/lib 
unix: QMAKE_RPATHLINKDIR += $${usr_path}/lib
